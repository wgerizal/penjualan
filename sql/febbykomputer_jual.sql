-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03 Feb 2018 pada 13.50
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `febbykomputer_jual`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `id_supplier` int(11) DEFAULT NULL,
  `id_kategori_barang` int(11) DEFAULT NULL,
  `id_stok_barang` int(11) DEFAULT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `keterangan_barang` text NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id_barang`, `id_supplier`, `id_kategori_barang`, `id_stok_barang`, `nama_barang`, `harga_barang`, `keterangan_barang`, `gambar`) VALUES
(4, 3, 3, 4, 'Asus GT 630 Silent 1Gb', 450000, 'Asus GT 630 Silent 1Gb 64Bit DDR3 GT630-1GB3-L NVidia PCI Exp.', '136015a52916360450720110707.png'),
(5, 4, 3, 5, 'Nvdia gforce GTX 640 ', 150000, 'Nvdia gforce GTX  640 Cuda Cores 1000 MHz, Memory Clock Speed 7000 MHz', '238785a5291d6dace1259223742.png'),
(6, 3, 3, 8, 'NVIDIA GeForce GTX 980  Core', 450000, 'NVIDIA GeForce GTX 980  Core Clocks 1000 MHz / 1075 MHz 2 Gb', '234405a5293c8a511c949959439.png'),
(7, 7, 3, 9, 'Avexir Core Series DDR4 8GB ', 800000, 'Avexir Core Series DDR4 Dual Channel 8GB Dual Channel clock speed 5600 Mhz', '297485a52941392964674133429.png'),
(8, 8, 3, 10, 'Corsair DDR3 PC12800 1600Mhz', 450000, 'Corsair DDR3 PC12800  Kapasitas 8GB  clock memory 2400 Mhz', '13925a5294f6e7abf263031613.png'),
(9, 3, 3, 11, 'NVidia  Crucial DDR3L 4GB', 600000, 'NVidia  Crucial PC15000 DDR3L 4GB (1X4GB) clock speed 2400 Mhz', '143575a5295465fd34155725948.png'),
(10, 10, 3, 12, 'Galax Geforce GT 30 Exoc 1 GB', 550000, 'Galax Geforce GT 30 Exoc (Extreme Overclock) 1 GB DDR5 clock speed 5600 Mhz', '223515a5295b022b74219963516.png'),
(11, 11, 3, 13, 'Nvdia quadro fx 380 DDR 3 PCI 12000 ', 350000, 'Nvdia quadro fx 380 DDR 3 PCI 12000 clock speed 3200 Mhz', '283805a52961abdfc6680897460.png'),
(12, 3, 2, 14, 'Avexir Core Series DDR4 8GB', 500000, 'Avexir Core Series DDR4 8GB Dual Channel clock speed 5600 Mhz', '6055a529a0e65461237669942.png'),
(13, 4, 2, 15, 'Corsair 8GB DDR3', 350000, 'Corsair 8GB DDR3 PC12800 1600Mhz', '84255a529a6759863477939394.png'),
(14, 5, 2, 16, 'Corsair CMZ16GX  16GB', 250000, 'Corsair CMZ16GX3M2A1600C10B 16GB DDR3', '126545a529aa05b74e044882082.png'),
(15, 6, 2, 17, 'Corsair Vengeance 16GB', 340000, 'Corsair Vengeance 16GB (2X8GB) DDR4 PC21000', '163995a529ac4ecb50137835166.png'),
(16, 7, 2, 18, 'Corsair Vengeance LPX 8GB ', 450000, 'Corsair Vengeance LPX 8GB (2X4GB) DDR4 PC21000', '20025a529af0e57ca528733165.png'),
(17, 8, 2, 19, 'Corsair Vengeance Pro 16GB', 550000, 'Corsair Vengeance Pro 16GB (2X8GB) DDR3 PC12800', '1635a529b1bb9a0b603453720.png'),
(18, 9, 2, 20, 'Crucial PC15000 DDR3L ', 450000, 'Crucial PC15000 DDR3L 4GB (1X4GB)', '80965a529b3b6b52f679675463.png'),
(19, 3, 2, 21, 'Kingston 2GB DDR3', 330000, 'Kingston 2GB DDR3 PC10600 1333MHz', '264575a529b561caf3938390851.png'),
(20, 10, 2, 22, 'Kingston 4GB DDR3 ', 430000, 'Kingston 4GB DDR3 PC12800 1600MHz', '202545a529b7340eb4493022046.png'),
(21, 11, 2, 23, 'Kingston 8GB DDR3', 650000, 'Kingston 8GB DDR3 PC12800 1600MHz', '10235a529bb60adb4066517325.png'),
(22, 12, 2, 24, 'Team Elite Plus 4GB DDR3', 320000, 'Team Elite Plus TPD34G1600HC1101 4GB DDR3', '55805a529bdd411d7245933543.png'),
(23, 3, 2, 25, 'Team Vulcan 8GB', 350000, 'Team Vulcan TLRED38G1600HC9DCC01 8GB', '82135a529c002c1e4131165360.png'),
(24, 3, 2, 26, 'Team Xtreem Dark 8GB', 550000, 'Team Xtreem Dark TDRED48G3000HC16ADC01 8GB', '161705a529c19b7a2b189789447.png'),
(25, 3, 2, 27, 'VenomRX 4GB DDR3', 420000, 'VenomRX 4GB DDR3 PC1333', '248595a529c33a8dc7434269933.png'),
(26, 3, 2, 28, 'V-Gen 4GB DDR3 PC10600', 510000, 'V-Gen  DDR3 PC10600 Kapasitas 4 GB', '170805a529c643fd39203088784.png'),
(27, 5, 2, 29, 'V-Gen 8GB DDR3 PC12800', 220000, 'V-Gen  DDR3 PC12800 Kapasitas 8 GB', '103855a529cb660b62622646445.png'),
(28, 7, 2, 30, 'VISIPRO 2GB DDR3 PC10600', 335600, 'VISIPRO 2GB DDR3 PC10600  Kapasitas 2 GB', '92065a529ce900019714080911.png'),
(29, 3, 2, 31, 'VISIPRO 2GB DDR3 PC12800', 230000, 'VISIPRO DDR3 PC12800 kapasitas 2GB', '296875a529d0d3adca908709205.png'),
(30, 11, 2, 32, 'VISIPRO  4GB DDR3', 350000, 'VISIPRO SO-DIMM 4GB DDR3 PC12800', '210925a529d3e35713021945869.png'),
(31, 3, 4, 33, 'WDC SATA III Green 500Gb', 550000, 'WDC SATA III Green 500Gb Int 3,5 Inch', '87115a529ff8d807c131929206.png'),
(32, 4, 4, 34, 'WD Blue 3TB', 650000, 'WD Blue 3TB Desktop Hard Disk Drive', '276215a52a03ba18b0524585891.png'),
(33, 3, 4, 35, 'Toshiba Sata III 2 TB', 670000, 'Toshiba Sata III 2 TB 7200rpm Int 3,5 Inch', '262985a52a05dbd5cf502981063.png'),
(34, 4, 4, 36, 'Toshiba Sata III 1 TB', 620000, 'Toshiba Sata III 1 TB 7200rpm 3.5Inch Int 3,5 Inch', '65075a52a0780c7be857678543.png'),
(35, 3, 4, 37, 'SEAGATE Skyhawk 2TB', 700000, 'SEAGATE Skyhawk 2TB T2000VX008', '73685a52a098ad18a935956286.png'),
(36, 3, 4, 38, 'Seagate IronWolf 6 TB', 850000, 'Seagate IronWolf 6 TB 3.5 inch Internal Hard Drive for 1-8 Bay NAS Systems', '249295a52a0c833337792085671.png'),
(37, 3, 4, 39, 'Hitachi SATA III 1TB', 540000, 'Hitachi SATA III 1TB 3.5Inch Int 3,5 Inch', '134465a52a0e374c46262059019.png'),
(38, 3, 4, 40, 'Hitachi 3 TB', 580000, 'Hitachi 3 TB 7200 RPM 64Mb For NAS Int 3,5 Inch', '249035a52a10e5a05c295674760.png'),
(39, 3, 4, 41, 'harddisk-ssd 1 TB', 3450000, 'harddisk-ssd 1 TB SATA int 3.5 inch', '157325a52a169bcf48835541538.png'),
(40, 3, 4, 42, 'Harddisk-IDE 2 TB', 430000, 'Harddisk-IDE 2 TB SATA int 3.5 inch', '129575a52a1abe9949442308433.png'),
(41, 3, 5, 43, 'Intel Xeon E5620', 50000, 'Intel Xeon E5620', '305065a52a3c59c36b941642590.png'),
(42, 4, 5, 44, 'Intel Pentium G840', 650000, 'Intel Pentium G840', '146195a52a3de0b2c2212806275.png'),
(43, 4, 5, 45, 'Intel Pentium G840', 430000, 'Intel Pentium G840', '150325a52a498a5655343958029.png'),
(44, 4, 5, 46, 'Intel Pentium G645', 550000, 'Intel Pentium G645', '165295a52a52226214727895300.png'),
(45, 4, 5, 47, 'Intel Pentium Dual-Core G3260', 430000, 'Intel Pentium Dual-Core G3260', '155655a52a54ba126b780624926.png'),
(46, 4, 5, 48, 'Intel Pentium Dual-Core E5700', 700000, 'Intel Pentium Dual-Core E5700', '24345a52a5680aea5745257633.png'),
(47, 4, 5, 49, 'Intel Core i7-6700', 650000, 'Intel Core i7-6700', '235425a52a5884b673910659896.png'),
(48, 4, 5, 50, 'Intel Core i7-4790k', 600000, 'Intel Core i7-4790k', '194475a52a5a3ab4b6565284164.png'),
(49, 3, 5, 51, 'Intel Core i5-6600', 760000, 'Intel Core i5-6600', '190445a52a5b9bf695078036053.png'),
(50, 5, 5, 52, 'Intel Core i5-6500', 650000, 'Intel Core i5-6500', '217095a52a6118b3ae893277071.png'),
(51, 5, 5, 53, 'Intel Core i5-6400', 760000, 'Intel Core i5-6400', '167705a52a646eae0d713573115.png'),
(52, 4, 5, 54, 'Intel Core i3-2120', 450000, 'Intel Core i3-2120', '124355a52a66ae68a5869708210.png'),
(53, 5, 5, 55, 'Intel Core i3-2100', 440000, 'Intel Core i3-2100', '104485a52a69ceb0fb397882342.png'),
(54, 5, 5, 56, 'Intel Core 2 Quad Q9300', 340000, 'Intel Core 2 Quad Q9300   Tipe LGA 11.51 Kecepatan 8.3 Ghz Core Dual', '288735a52a6c7ad6a5681631797.png'),
(55, 5, 5, 57, 'AMD Ryzen 7 1800X', 540000, 'AMD Ryzen 7 1800X Tipe LGA 14.51 Kecepatan 7.7 Ghz Core Dual', '296465a52a6edc220c276798659.png'),
(56, 4, 5, 58, 'AMD Ryzen 7 1700', 550000, 'AMD Ryzen 7 1700 Tipe LGA 11.51 Kecepatan 7.7 Ghz Core Dual', '48825a52a8ad16122301956192.png'),
(57, 5, 5, 59, 'AMD Athlon II X4 630', 650000, 'AMD Athlon II X4 630 Tipe LGA 11.51 Kecepatan 3.7 Ghz Core Dual', '191715a52a8f29d6c3785619559.png'),
(58, 10, 6, 60, 'ASRock B150M', 560000, 'ASRock B150M PRO4 Intel Socket 1151 ', '3959880505a5d149157bd0325490975.png'),
(59, 5, 6, 61, 'ASRock X399 Taichi', 456000, 'ASRock X399 Taichi AMD Socket TR4', '16110449005a5e150680cf5433483566.png'),
(60, 3, 6, 62, 'Asus A88X (Bolton D4)', 456000, 'Asus A88X (Bolton D4) (FM2+) CROSSBLADE RANGER AMD Socket FM2', '7893935485a5e156142653643802353.png'),
(61, 3, 6, 63, 'Asus E3M-ET V5', 450000, 'Asus E3M-ET V5 DDR4 Intel Socket 1151', '12577037575a5e15959a61e803661344.png'),
(62, 3, 6, 64, 'Asus EX-A320M', 800000, 'Asus EX-A320M-Gaming AMD Socket AM4', '12093917745a5e15bfb651a710304743.png'),
(63, 9, 6, 65, 'Asus ROG', 700000, 'Asus ROG CROSSHAIR VI Hero AMD Socket AM4', '13067782715a5e16077b082296051799.png'),
(64, 3, 6, 66, 'Fatal1ty Z170', 900000, 'Fatal1ty Z170 Gaming K6+(L3)', '11191382925a5e163150c31815040868.png'),
(65, 4, 7, 67, 'Ace Power Cobra Black', 7900000, 'Ace Power Cobra Black + Psu 400W', '10525722475a5e46ec0c87b938582635.png'),
(66, 5, 7, 68, 'Ace Power Tapir', 500000, 'Ace Power Tapir + Psu 400W Black', '21164129405a5e474820871027744713.png'),
(67, 6, 7, 69, 'Ace Power Tapir', 569000, 'Ace Power Tapir + Psu 400W Red Micro ATX', '6170274225a5e4a4a6a99d883075111.png'),
(68, 8, 7, 70, 'Aerocool DS Cube', 700000, 'Aerocool DS Cube Window Blue Green Pink Micro ATX', '1290612955a5e4a779383d647124090.png'),
(69, 11, 8, 71, 'Bitfenix Whisper 850W', 230000, 'Bitfenix Whisper 850W - BWG850M Full Modular - 80+ GOLD (By Alfa AAA)', '18424839985a5e4b5f55e0a297183037.png'),
(70, 9, 8, 72, 'Andyson AD-R1200EMA', 700000, 'Andyson AD-R1200EMA 1200W Platinum', '2881941325a5e4ba07690b299245822.png'),
(71, 12, 8, 73, 'Cooler Master V1200W', 320000, 'Cooler Master V1200W Full Modular Cable', '2044609455a5e4bc639081552285554.png'),
(72, 3, 7, 74, 'Corsair AX1500i', 450000, 'Corsair AX1500i (CP-9020057-EU) 1500W AX Series Digital (Fully Modular)', '1245178595a5e4be70f0e4539390589.png'),
(73, 3, 9, 75, 'Asus R.O.G Claymore', 760000, 'Asus R.O.G Claymore + Numpad Keyboard', '17331952365a5e4c617b398227199112.png'),
(74, 6, 9, 76, 'Bloody B640', 110000, 'Bloody B640 (Infrared Switch Mechanical Keyboard) Keyboard', '15466229965a5e4cbd6540e911232781.png'),
(75, 8, 9, 77, 'Cooler Master Novatouch', 320000, 'Cooler Master Novatouch TKL Keyboard', '7316224255a5e4d2882e0c067203117.png'),
(76, 6, 10, 78, 'Corsair Glaive RGB', 50000, 'Corsair Glaive RGB Alumunium Gaming Mouse', '13614166905a5e4da82e7bf916313707.png'),
(77, 8, 10, 79, 'Corsair Scimitar Pro', 100000, 'Corsair Scimitar Pro RGB Gaming BlackYellow Mouse', '9634868155a5e4dd1d4cf5993492769.png'),
(78, 11, 10, 80, 'Gigabyte Krypton', 110000, 'Gigabyte Krypton Wired Mouse', '821937935a5e4e0d95279257966196.png'),
(79, 11, 11, 81, 'Matsunaga 500W', 400000, 'Matsunaga 500W Motor Stabiliser', '681733235a5e4e4da0b1f628911529.png'),
(80, 8, 11, 82, 'ICA  FR 1000', 150000, 'ICA  FR 1000 - 1000 VA  Stabil', '19450007675a5e4e77267d4437550506.png'),
(81, 3, 11, 83, 'Minamoto  SM 15K3', 500000, 'Minamoto  SM 15K3 - 15 KVA  Stabilizier', '18433489825a5e4ea5ac12d574116137.png'),
(82, 3, 12, 84, 'Acer 15.6 Inch', 900000, 'Acer 15.6 Inch P166HQL LED Size 15 inch', '14404801605a5e4ed1a3993664962630.png'),
(83, 3, 12, 85, 'AOC 15.6 Inch ', 890000, 'AOC 15.6 Inch E1670SWU (USB Powered) Size 15 inch', '1938531405a5e4ef0f34db030861031.png'),
(84, 3, 12, 86, 'Asus 16 Inch VH168D LED Size 15 inch', 980000, 'Asus 16 Inch VH168D LED Size 15 inch', '6845790715a5e4f133b7bd876380686.png'),
(85, 3, 13, 87, 'Bose Acoustimass AM', 65000, 'Bose Acoustimass AM 10 Series V (Black White) 5.1 Aktif', '8493983575a5e4f5996250451850182.png'),
(86, 3, 13, 88, 'Altec Lansing', 870000, 'Altec Lansing BXR 1321 Aktif', '14897083815a5e4f8333c63909995963.png'),
(87, 10, 13, 89, 'Audiobox Beatbox 6000', 120000, 'Audiobox Beatbox 6000', '19279351745a5e4fa7d1f76669615566.png'),
(88, 3, 14, 90, 'Brother DCP-1601', 790000, 'Brother DCP-1601 Laser Mono', '6385802665a5e4fd96eecb872823996.png'),
(89, 3, 14, 91, 'Canon G2000', 879000, 'Canon G2000 Print,Scan,Copy Multifunction', '12740211425a5e50006ad22614093320.png'),
(90, 3, 14, 92, 'Fuji Xerox ', 760000, 'CM215B Print,Scan,Copy,Colour Multifunction', '8114661075a5e50292fbd9487496694.png'),
(91, 3, 2, 93, 'Avexir Core Series DDR4 8GB', 500000, 'Avexir Core Series DDR4 8GB Dual Channel clock speed 5600 Mhz', '12903701145a5eef08a997c935891140.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pemesanan`
--

CREATE TABLE `detail_pemesanan` (
  `id_detail_pemesanan` int(11) NOT NULL,
  `id_pemesanan` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pemesanan`
--

INSERT INTO `detail_pemesanan` (`id_detail_pemesanan`, `id_pemesanan`, `id_barang`, `jumlah`) VALUES
(1, NULL, NULL, 1),
(2, NULL, NULL, 1),
(3, NULL, NULL, 1),
(4, 4, NULL, 1),
(5, 5, 14, 1),
(6, 6, 12, 1),
(7, 6, 13, 2),
(8, 6, 14, 2),
(9, 7, 17, 1),
(10, 7, 5, 1),
(11, 7, 37, 1),
(12, 7, 48, 1),
(13, 8, 14, 2),
(14, 8, 18, 2),
(15, 9, 20, 5),
(16, 10, 5, 1),
(17, 10, 33, 1),
(18, 11, 12, 2),
(19, 11, 19, 3),
(20, 12, 19, 1),
(21, 12, 35, 4),
(22, 13, 19, 1),
(23, 13, 29, 1),
(24, 17, 19, 1),
(25, 18, 29, 1),
(26, 19, 23, 1),
(27, 20, 19, 1),
(28, 21, 57, 2),
(29, 22, 23, 1),
(30, 23, 23, 1),
(31, 24, 23, 1),
(32, 25, 19, 1),
(33, 26, 23, 1),
(34, 27, 58, 3),
(35, 28, 19, 1),
(36, 29, 19, 1),
(37, 30, 19, 1),
(38, 31, 23, 1),
(39, 32, 86, 3),
(40, 33, 12, 1),
(41, 34, 12, 1),
(42, 35, 19, 1),
(43, 37, 61, 1),
(44, 39, 12, 1),
(45, 40, 19, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_barang`
--

CREATE TABLE `kategori_barang` (
  `id_kategori_barang` int(11) NOT NULL,
  `nama_kategori` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_barang`
--

INSERT INTO `kategori_barang` (`id_kategori_barang`, `nama_kategori`) VALUES
(2, 'RAM'),
(3, 'VGA'),
(4, 'Harddisk'),
(5, 'Processor'),
(6, 'Motherboard'),
(7, 'Casing'),
(8, 'Power Supply'),
(9, 'Keyborad'),
(10, 'Mouse'),
(11, 'Stabiliser'),
(12, 'Monitor'),
(13, 'Speaker'),
(14, 'Printer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konsumen`
--

CREATE TABLE `konsumen` (
  `id_konsumen` int(11) NOT NULL,
  `id_pengguna` int(11) DEFAULT NULL,
  `nama_konsumen` varchar(50) NOT NULL,
  `alamat_konsumen` varchar(150) NOT NULL,
  `notelp_konsumen` int(20) NOT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `atas_nama` varchar(50) DEFAULT NULL,
  `norek` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konsumen`
--

INSERT INTO `konsumen` (`id_konsumen`, `id_pengguna`, `nama_konsumen`, `alamat_konsumen`, `notelp_konsumen`, `bank`, `atas_nama`, `norek`) VALUES
(4, 12, 'Rendi', 'Jl. jalan', 2147483647, NULL, NULL, NULL),
(5, 13, 'Dudi', 'Jl. jalan', 2147483647, NULL, NULL, NULL),
(6, 14, 'Teri', 'Teri', 2147483647, NULL, NULL, NULL),
(7, NULL, 'nanda', 'Jl. jalan', 2147483647, 'BCA', 'Rizki Nanda Saputra', 2147483647),
(8, 16, 'Rendi', 'Jl. Alamat', 2147483647, NULL, NULL, NULL),
(9, NULL, 'rizki nanda saputra', 'jl terusan permai 25 cci 1 no 12 margahayu permai kopo bandung', 2147483647, 'BCA', 'Rizki Nanda Saputra', 897687969),
(10, 18, 'rizki nanda saputra', 'jl terusan permai 25 cci 1 no 12 margahayu permai kopo bandung', 2147483647, 'BCA', 'Maryadi Putra', 980698009),
(11, 19, 'Yadi Mulyadi', 'Babakan Citarip', 876787578, 'BRI', 'Yadi Mulyadi', 7689864),
(12, 20, 'Dolly Marthin', 'Citarip City', 2147483647, 'Mandiri', 'Dolly Marthin', 98796554),
(13, 21, 'Dolly Marthin', 'Citarip City', 2147483647, 'BCA', 'Maryadi Putra', 897687969),
(14, 22, 'dadang', 'kopo', 876787657, NULL, NULL, NULL),
(15, 23, 'dadang', 'kopo', 897887766, 'BCA', 'DADANG', 98340995),
(16, 24, 'gunawan', 'banjaran timur', 82221123, 'BNI', 'anang', 2147483647),
(17, 25, 'Efza', 'Jl. Sekeloa', 2147483647, 'BNI', 'Efza ', 1212121),
(18, 26, 'rizki nanda saputra', 'Margahayu Permai Kopo', 2147483647, 'BCA', 'Rizki Nanda', 911225522),
(19, 27, 'wagerizal', 'cicukang indah 3 no 5', 2147483647, 'bni', 'wage', 123123);

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_stok`
--

CREATE TABLE `log_stok` (
  `id_log` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `status_stok` enum('masuk','keluar','','') NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `id_konsumen` int(11) DEFAULT NULL,
  `kode_pemesanan` varchar(20) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `jenis_pengiriman` varchar(50) NOT NULL,
  `status_pengiriman` varchar(50) DEFAULT NULL,
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `bukti_pembayaran` varchar(150) DEFAULT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pemesanan`
--

INSERT INTO `pemesanan` (`id_pemesanan`, `id_konsumen`, `kode_pemesanan`, `total_bayar`, `jenis_pengiriman`, `status_pengiriman`, `status_pembayaran`, `bukti_pembayaran`, `tanggal`) VALUES
(4, 4, 'aXksoQ', 5000, 'COD', '', 'Belum Lunas', '', '2018-01-07'),
(5, 9, '4LVB81', 250000, 'Pengiriman Online', 'Sudah Dikirim', 'Lunas', '705a529f586ec34490722617.JPG', '2018-01-07'),
(6, 10, 'DADuTb', 1700000, 'Pengiriman Online', 'Sudah Dikirim', 'Lunas', '117045a52af07586d9031465349.JPG', '2018-01-08'),
(7, 11, 'wGXAI1', 1840000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-08'),
(8, 12, 'czkQbn', 1400000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-08'),
(9, 13, 'SfNInY', 2150000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-08'),
(10, 13, 'BcL1gi', 820000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-08'),
(11, 10, 'MHTfMv', 1990000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '259465a5876db908f7842108903.jpeg', '2018-01-12'),
(12, 15, 'VryRwW', 3130000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-13'),
(13, 10, '1klk7U', 560000, 'Pengiriman Online', 'Sudah Dikirim', 'Lunas', '54135a59cccaed998202944534.jpeg', '2018-01-13'),
(17, 10, 'ZCo9SO', 330000, 'COD', '', 'Belum Lunas', '', '2018-01-13'),
(18, 10, 'GKZEIA', 230000, 'COD', '', 'Belum Lunas', '', '2018-01-13'),
(19, 10, 'hIf0d5', 350000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-13'),
(20, 10, 'tRMn7u', 330000, 'COD', '', 'Belum Lunas', '', '2018-01-13'),
(21, 16, 'RvrlfR', 1300000, 'Pengiriman Online', 'Sudah Dikirim', 'Lunas', '112965a59da8b2507a748501688.jpeg', '2018-01-13'),
(22, 10, 'uXB7uj', 350000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-15'),
(23, 10, 'MK70yU', 350000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-15'),
(24, 10, 'UhMC3U', 350000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-15'),
(25, 10, 'QRjUTy', 330000, 'COD', '', 'Lunas', '', '2018-01-15'),
(26, 10, 'e8jKck', 350000, 'Pengiriman Online', 'Sudah Dikirim', 'Lunas', '15157702275a5ce62126096365752654.JPG', '2018-01-15'),
(27, 10, 'hpcibx', 1680000, 'Pengiriman Online', 'Sudah Dikirim', 'Lunas', '681972825a5d297f1224a589115279.JPG', '2018-01-15'),
(28, 17, '6zIEfa', 330000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-16'),
(29, 10, '4I6m69', 330000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-16'),
(30, 10, 'BZY2Sa', 330000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-16'),
(31, 10, 'YWNI3i', 350000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '19037636685a5e13b8a9e4a355634101.jpeg', '2018-01-16'),
(32, 18, 'N5Iehi', 2610000, 'Pengiriman Online', 'Sudah Dikirim', 'Lunas', '20927382685a5ed022688c4421153536.jpg', '2018-01-17'),
(33, 10, 'vToyXM', 500000, 'Pengiriman Online', 'Menunggu', 'Lunas', '13636641775a5ef0e9777ac754354314.jpg', '2018-01-17'),
(34, 10, 'C6sJnP', 500000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-20'),
(35, 10, '1cyeuD', 330000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-21'),
(36, 10, 'NzTd4P', 0, 'COD', '', 'Belum Lunas', '', '2018-01-21'),
(37, 10, 'upMHFG', 450000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-01-21'),
(38, 19, 'EibzE0', 0, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-02-03'),
(39, 19, '2cXqks', 500000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-02-03'),
(40, 19, 'bgoNpJ', 330000, 'Pengiriman Online', 'Menunggu', 'Belum Lunas', '', '2018-02-03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `level_pengguna` varchar(100) NOT NULL,
  `status_pengguna` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `username`, `email`, `password`, `level_pengguna`, `status_pengguna`) VALUES
(6, 'Admin', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'admin', 1),
(7, 'Pemilik Toko', 'toko@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'pemilik', 1),
(12, 'rendi', 'rendi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(13, 'dudi', 'dudi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(14, 'teri', 'teri@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(16, 'rendi', 'rendi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(18, 'rizki nanda', 'rizkinanda15@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(19, 'Yadi Mulyadi', 'Yadimyadi21@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(20, 'Dollmart', 'dmarthin@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(21, 'Dollmart', 'dollmaret@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(22, 'dadang', 'dadang@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(23, 'dadang suradang', 'dadang@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(24, 'anjasnara', 'gunawan.anjas@ymail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(25, 'agastya14', 'agastya14.efza@gmail.com', '3ecf9cad9a60652fcc8c09dfaef4c033', 'konsumen', 1),
(26, 'rizkinanda', 'rizkinanda15@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'konsumen', 1),
(27, 'wgerizal', 'wgerizal@gmail.com', '212877739b649c282db17cfe536e9a61', 'konsumen', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `servis`
--

CREATE TABLE `servis` (
  `id_servis` int(11) NOT NULL,
  `id_konsumen` int(11) DEFAULT NULL,
  `id_ketegori_barang` int(11) DEFAULT NULL,
  `layanan_servis` varchar(50) NOT NULL,
  `keluhan` text NOT NULL,
  `status_servis` varchar(50) NOT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `servis`
--

INSERT INTO `servis` (`id_servis`, `id_konsumen`, `id_ketegori_barang`, `layanan_servis`, `keluhan`, `status_servis`, `tanggal`) VALUES
(3, 10, 2, 'Datang Ke Toko', 'bad sector', 'Sudah Selesai', '2018-01-08'),
(4, 10, 4, 'Panggil Ke Tempat', 'boot loop', 'Belum Selesai', '2018-01-08'),
(5, 10, 3, 'Panggil Ke Tempat', 'tidak muncul ke monitor', 'Belum Selesai', '2018-01-08'),
(6, 12, 5, 'Panggil Ke Tempat', 'cepat panas sehingga komputer tiba tiba mati', 'Belum Selesai', '2018-01-08'),
(7, 11, 2, 'Panggil Ke Tempat', 'komputer gak nyala', 'Belum Selesai', '2018-01-08'),
(8, 11, 3, 'Panggil Ke Tempat', 'gak muncul di layar', 'Belum Selesai', '2018-01-08'),
(11, 14, NULL, 'Panggil Ke Tempat', 'blank scree', 'Belum Selesai', '0000-00-00'),
(12, 10, NULL, 'Panggil Ke Tempat', 'cepat panas', 'Sudah Selesai', '2018-01-15'),
(13, 10, NULL, 'Panggil Ke Tempat', 'tidak bisa menampilkan ke layar monitor', 'Belum Selesai', '2018-01-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok_barang`
--

CREATE TABLE `stok_barang` (
  `id_stok_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stok_barang`
--

INSERT INTO `stok_barang` (`id_stok_barang`, `jumlah`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0),
(84, 0),
(85, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(50) NOT NULL,
  `alamat_supplier` varchar(150) NOT NULL,
  `notelp_supplier` varchar(20) NOT NULL,
  `email_supplier` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `alamat_supplier`, `notelp_supplier`, `email_supplier`) VALUES
(3, 'abdul', 'Jl terusan buah batu', '0833223311', 'abdul@gmail.com'),
(4, 'Qodir', 'banjaran', '0896586554', 'Qodir@gmail.com'),
(5, 'Zaelani', 'banjaran', '0896586554', 'Zaelani@gmail.com'),
(6, 'Dika', 'Jl terusan buah batu', '08122344321', 'Dika@gmail.com'),
(7, 'Fajar', 'Jl Terusan Kopo Katapang', '0813224433', 'Fajar@gmail.com'),
(8, 'Ahdmad Zaeri', 'Jl terusan cibaduyut', '08522234356', 'Ahmadzaeri@gmail.com'),
(9, 'Gunawan', 'Jl Raya Banjaran', '0899877667', 'Gunawan@gmail.com'),
(10, 'Anjas', 'Jl Raya Banjaran', '0863452134', 'anjas@gmail.com'),
(11, 'Agus Sulaiman', 'Jl Terusan Kopo Katapang', '0823322554', 'Agussulaiman@gmail.com'),
(12, 'Dani', 'Antapani', '0899877667', 'Dani@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_supplier` (`id_supplier`,`id_kategori_barang`,`id_stok_barang`),
  ADD KEY `id_stok_barang` (`id_stok_barang`),
  ADD KEY `id_kategori_barang` (`id_kategori_barang`);

--
-- Indexes for table `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  ADD PRIMARY KEY (`id_detail_pemesanan`),
  ADD KEY `id_pemesanan` (`id_pemesanan`,`id_barang`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  ADD PRIMARY KEY (`id_kategori_barang`);

--
-- Indexes for table `konsumen`
--
ALTER TABLE `konsumen`
  ADD PRIMARY KEY (`id_konsumen`),
  ADD KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `log_stok`
--
ALTER TABLE `log_stok`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`),
  ADD KEY `id_konsumen` (`id_konsumen`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `servis`
--
ALTER TABLE `servis`
  ADD PRIMARY KEY (`id_servis`),
  ADD KEY `id_konsumen` (`id_konsumen`,`id_ketegori_barang`),
  ADD KEY `id_ketegori_barang` (`id_ketegori_barang`);

--
-- Indexes for table `stok_barang`
--
ALTER TABLE `stok_barang`
  ADD PRIMARY KEY (`id_stok_barang`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  MODIFY `id_detail_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  MODIFY `id_kategori_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `konsumen`
--
ALTER TABLE `konsumen`
  MODIFY `id_konsumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `log_stok`
--
ALTER TABLE `log_stok`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `servis`
--
ALTER TABLE `servis`
  MODIFY `id_servis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `stok_barang`
--
ALTER TABLE `stok_barang`
  MODIFY `id_stok_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`id_stok_barang`) REFERENCES `stok_barang` (`id_stok_barang`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`id_kategori_barang`) REFERENCES `kategori_barang` (`id_kategori_barang`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `barang_ibfk_3` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ketidakleluasaan untuk tabel `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  ADD CONSTRAINT `detail_pemesanan_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `detail_pemesanan_ibfk_2` FOREIGN KEY (`id_pemesanan`) REFERENCES `pemesanan` (`id_pemesanan`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ketidakleluasaan untuk tabel `konsumen`
--
ALTER TABLE `konsumen`
  ADD CONSTRAINT `konsumen_ibfk_1` FOREIGN KEY (`id_pengguna`) REFERENCES `pengguna` (`id_pengguna`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ketidakleluasaan untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD CONSTRAINT `pemesanan_ibfk_1` FOREIGN KEY (`id_konsumen`) REFERENCES `konsumen` (`id_konsumen`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ketidakleluasaan untuk tabel `servis`
--
ALTER TABLE `servis`
  ADD CONSTRAINT `servis_ibfk_1` FOREIGN KEY (`id_konsumen`) REFERENCES `konsumen` (`id_konsumen`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `servis_ibfk_2` FOREIGN KEY (`id_ketegori_barang`) REFERENCES `kategori_barang` (`id_kategori_barang`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
