<?php
	include "Controller.php";
	
	class KategoriBarangController extends Controller{

		private $title = 'Kategori Barang';
		private $controller = 'KategoriBarangController.php';
		public $folder = 'kategori-barang';
		public $path_controller;

		public $table_main = 'KategoriBarang';
		public $table_data = 'kategoriBarang';
    	public $table_primary = 'id_kategori_barang';
    	public $table_title = array(
									'Nama Kategori',
								);
    	public $table_field = array(
									'nama_kategori',
    							);

		public $model_main;		
    	

		function __construct()
	    {
	    	$this->auth();
	    	$this->path_controller = "../../controller/".$this->controller;
	    	$this->model_main = $this->model($this->table_main);
	    }

	    public function index(){
	    	$data['title'] = $this->title;
	     	$data[$this->table_data] = $this->model_main->getDataAll();
	    	return $data;
	    }

	    public function add(){
	    	$data['title'] = $this->title;
	    	return $data;
	    }

	    public function edit(){
	    	$data['title'] = $this->title;
	    	$data[$this->table_data] = $this->model_main->getDataByID($_GET["id"]);
	    	return $data;
	    }

	    public function delete(){
	    	$message = 'Deleted data failed.';
		   	$result = 'failed';
	     	$data[$this->table_primary] = $_POST["id"];

	    	if ($this->model_main->data_delete($data)) {
	    		$message = 'Deleted data successfully.';
	    		$result = 'success';	    		
	    	}

	    	if (session_status() == PHP_SESSION_NONE) {
		        session_start();
		    }

	    	$_SESSION["notification_message"] = $message;
		    $_SESSION["notification_result"] = $result;

			echo 'true';

	    }

	    public function detail(){
	    	$data['title'] = $this->title;
	     	$data[$this->table_data] = $this->model_main->getDataAll($_GET["id"]);
	    	return $data;
	    }

	    public function save(){

	    	$message = 'Added data failed.';
		   	$result = 'failed';
		   
		   	// form post
		   	$data = $_POST;
			
	     	if ($data['post_type'] === 'add') {
	     		if ($this->model_main->data_insert($data)) {
    				$message = 'Added data successfully.';
    				$result = 'success';		
    			}
	     	}else{

	     		if ($this->model_main->data_edit($data)) {
 					$message = 'Edit data successfully.';
    				$result = 'success';
 				}else{
		    		$message = 'Edit data failed.';
		    	}

	     	}

	     	if (session_status() == PHP_SESSION_NONE) {
		        session_start();
		    }

		    $_SESSION["notification_message"] = $message;
		    $_SESSION["notification_result"] = $result;

		    header("Location:../view/".$this->folder);

	    }
	}

	$KategoriBarangController = new KategoriBarangController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($KategoriBarangController, $_GET['func']));
	}
	if (isset($_POST['func']) && !empty($_POST['func'])) {
		call_user_func(array($KategoriBarangController, $_POST['func']));
	}

?>