<?php

	

	if (file_exists(dirname(__FILE__)."/../../helper/Helper.php")) { 

        include_once dirname(__FILE__)."/../../helper/Helper.php";

    }



	require_once dirname(__FILE__).'/../../vendor/autoload.php';



	use Spipu\Html2Pdf\Html2Pdf;

	use Spipu\Html2Pdf\Exception\Html2PdfException;

	use Spipu\Html2Pdf\Exception\ExceptionFormatter;



	include "Controller.php";

	

	class LaporanController extends Controller{



		private $title = 'Laporan';

		private $controller = 'LaporanController.php';

		public $folder = 'laporan';

		public $path_controller;



		public $table_main = 'Pemesanan';

		public $table_data = 'pemesanan';

    	public $table_primary = 'id_pemesanan';



		public $model_main;		

    	

    	public $helper;



		function __construct()

	    {

	    	$this->auth();

	    	$this->helper = new Helper();

	    	$this->path_controller = "../../controller/".$this->controller;

	    	$this->pemesanan = $this->model('Pemesanan');

	    	$this->servis = $this->model('Servis');

	    	$this->stok = $this->model('StokBarang');

	    	$this->logStok = $this->model('LogStok');

	    }



	    public function penjualan(){

	    	$data['title'] = "Laporan Penjualan Barang";

	    	return $data;

	    }



	    public function servis(){

	    	$data['title'] = "Laporan Servis Komputer";

	    	return $data;

	    }


	    public function stok(){

	    	$data['title'] = "Laporan Stok Komputer";

	    	return $data;

	    }


	    public function unduhLaporan(){



	    	$data = $_POST;

	    	$data['pemesanan'] = $this->pemesanan->getDataPemesananLaporan($data);

	    	$bulan = $this->helper->convertBulantoNama($data['fdate']);

	    	try {

			    ob_start();

			    include dirname(__FILE__).'/../view/pdf/laporan_penjualan.php';

			    $content = ob_get_clean();

			    $html2pdf = new Html2Pdf('L', 'A4', 'fr');

			    $html2pdf->writeHTML($content);

			    $html2pdf->output('LaporanPenjualanBarang.pdf');

			} catch (Html2PdfException $e) {

			    $formatter = new ExceptionFormatter($e);

			    echo $formatter->getHtmlMessage();

			}

	    }



	    public function unduhLaporanServis(){



	    	$data = $_POST;

	    	$data['servis'] = $this->servis->getDataServisLaporan($data);

	    	$bulan = $this->helper->convertBulantoNama($data['fdate']);

	    	try {

			    ob_start();

			    include dirname(__FILE__).'/../view/pdf/laporan_servis.php';

			    $content = ob_get_clean();

			    $html2pdf = new Html2Pdf('L', 'A4', 'fr');

			    $html2pdf->writeHTML($content);

			    $html2pdf->output('LaporanServis.pdf');

			} catch (Html2PdfException $e) {

			    $formatter = new ExceptionFormatter($e);

			    echo $formatter->getHtmlMessage();

			}

	    }
       public function unduhLaporanStok(){

	    	$data = $_POST;

	    	$data['stok'] = $this->logStok->getDataLaporanStok($data);

	    	$bulan = $this->helper->convertBulantoNama($data['fdate']);

	    	try {

			    ob_start();

			    include dirname(__FILE__).'/../view/pdf/laporan_stok.php';

			    $content = ob_get_clean();

			    $html2pdf = new Html2Pdf('L', 'A4', 'fr');

			    $html2pdf->writeHTML($content);

			    $html2pdf->output('LaporanStok.pdf');

			} catch (Html2PdfException $e) {

			    $formatter = new ExceptionFormatter($e);

			    echo $formatter->getHtmlMessage();

			}

	    }



	}



	$LaporanController = new LaporanController();

	if (isset($_GET['func']) && !empty($_GET['func'])) {

		call_user_func(array($LaporanController, $_GET['func']));

	}

	if (isset($_POST['func']) && !empty($_POST['func'])) {

		call_user_func(array($LaporanController, $_POST['func']));

	}



?>

