<?php
	include "Controller.php";
	
	class HalamanUtamaController extends Controller{

		function __construct()
	    {
	    	$this->auth();
	    }

	    public function index(){
	     	$data['title'] = '';
	    	$data['total_karyawan'] = 435;
	    	$data['total_jabatan'] = 5;
	    	$data['total_calon_karyawan'] = 35;
	    	return $data;
	    }

	}

	$HalamanUtamaController = new HalamanUtamaController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($HalamanUtamaController, $_GET['func']));
	}

?>