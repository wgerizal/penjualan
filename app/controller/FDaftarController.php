<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require '../../vendor/phpmailer/phpmailer/src/Exception.php';
	require '../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
	require '../../vendor/phpmailer/phpmailer/src/SMTP.php';
	include "Controller.php";
	
	class FDaftarController extends Controller{

		function __construct()
	    {
	    	if (session_status() == PHP_SESSION_NONE) {
		        session_start();
		    }
		    $this->servis = $this->model("Servis");	
	    	$this->barang = $this->model("Barang");	
	    	$this->konsumen = $this->model("Konsumen");	
	    	$this->pengguna = $this->model("Pengguna");	
	    	$this->pemesanan = $this->model("Pemesanan");	
	    	$this->detailPemesanan = $this->model("DetailPemesanan");	
	    	$this->kategoriBarang = $this->model("KategoriBarang");
	    	$this->stokBarang = $this->model("StokBarang");
	    	$this->logStok = $this->model("LogStok");
	    }

	    public function index(){
	     	$data['barang'] = $this->barang->getDataBarang();
	     	$data['kategoriBarang'] = $this->kategoriBarang->getDataAll();
	    	return $data;
	    }


	    public function daftar(){
	    	$result = false;		
	     	$data = $_POST;
	     	$data['level_pengguna'] = 'konsumen';
	     	$data['status_pengguna'] = '1';
	     	$data["password"]=md5(trim($data['password']));
	     	$data['id_pengguna'] = $this->pengguna->data_insert($data, true);
     		if ($data['id_pengguna'] != 0) {
     			$data['id_konsumen'] = $this->konsumen->data_insert($data, true);
     			if ($data['id_konsumen'] != 0) {
     				$result = true;		
     			}
     		}

		    if ($result) {
		    	$_SESSION['id_konsumen'] = $data['id_konsumen'];
		    	if (!empty($_SESSION['pemesanan'])) {
			    	header("Location:../view/utama/pembayaran.php");
			    }else if(!empty($_SESSION['servis'])){
			    	$data = $_SESSION['servis'];
			    	$data['id_konsumen'] = $_SESSION['id_konsumen'];
			    	$data['status_servis'] = 'Belum Selesai';

			    	$this->servis->data_insert($data);

			    	$data_konsumen = $this->konsumen->getDataKonsumenById($_SESSION['id_konsumen']);
			    	
			    	$_SESSION["notification_servis"] = 'success';

					header("Location:../view/utama/servis.php");
			    }else{
			    	$_SESSION["notification_daftar"] = 'success';
			    	header("Location:../view/utama/daftar.php");	
			    }
		    }else{
		    	$_SESSION["notification_daftar"] = 'failed';
		    	header("Location:../view/utama/daftar.php");
		    }		    
	    }

	    public function bayar(){
 			
 			$data = $_POST;
 			$data['id_konsumen'] = $_SESSION['id_konsumen'];
 			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 6; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }

 			$data['kode_pemesanan'] = $randomString;
 			$data['total_bayar'] = 0;
 			$jenis_pengiriman = $_SESSION['pemesanan']['jenis_pengiriman'];
	    	if ($jenis_pengiriman == 'online') {
	    		$jenis_pengiriman = "Pengiriman Online";
	    		$data['status_pengiriman'] = "Menunggu";
	    		$this->konsumen->updateBank($_SESSION['id_konsumen'],$data);
	    	}else{
	    		$jenis_pengiriman = "COD";
	    		$data['status_pengiriman'] = "";
	    	}
 			$data['jenis_pengiriman'] = $jenis_pengiriman;
 			$data['status_pembayaran'] = "Belum Lunas";
 			$data['bukti_pembayaran'] = "";
 			$data['tanggal'] = date('Y-m-d');
			$data['id_pemesanan'] = $this->pemesanan->data_insert($data, true);
     		if ($data['id_pemesanan'] != 0) {
     			ob_start();
     			$total = 0;
     			for ($i=0; $i < count($_SESSION['pemesanan']['id_barang']); $i++) { 
     				$data['id_barang'] = $_SESSION['pemesanan']['id_barang'][$i];
     				$data['jumlah'] = $_SESSION['pemesanan']['qty'][$i];
     				$barang = $this->barang->getDataByID($_SESSION['pemesanan']['id_barang'][$i]);
     				$id_stok = $barang[0]['id_stok_barang'];
     				$stok_barang = $this->stokBarang->getDataStokBarangById($id_stok);
     				$jumlah_stok = $stok_barang[0]['jumlah'];
     				$stok_berkurang = $jumlah_stok - $data['jumlah'];
     				$data_stok['id_stok_barang'] = $id_stok;
     				$data_stok['jumlah'] = $stok_berkurang;
     				$this->stokBarang->data_edit($data_stok);
     				if($data['jumlah'] != 0){
     				$data['status_stok'] = 'keluar';
	     			$this->logStok->data_insert($data);
     				}
     				if (count($barang) > 0) {
     					$total = $total + ($barang[0]['harga_barang'] * $_SESSION['pemesanan']['qty'][$i]);
     				}


     				if ($this->detailPemesanan->data_insert($data)) {
						$message = 'Added data successfully.';
						$result = 'success';		
					}		
     			}

     			$this->pemesanan->updateTotal($data['id_pemesanan'],$total);
     			
 				$data_konsumen = $this->konsumen->getDataKonsumenById($_SESSION['id_konsumen']);

 				//send mail
 				$mail             = new PHPMailer();

 				$mail->SMTPOptions = array(
				    'ssl' => array(
				        'verify_peer' => false,
				        'verify_peer_name' => false,
				        'allow_self_signed' => true
				    )
				);
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Host       = "mail.yourdomain.com"; // SMTP server
				$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
				                                           // 1 = errors and messages
				                                           // 2 = messages only
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "tls";                 
				$mail->Host       = "smtp.gmail.com";      // SMTP server
				$mail->Port       = 587;                   // SMTP port
				$mail->Username   = "febbykomputer@gmail.com";  // username
				$mail->Password   = "Rbisd$01";            // password

				$mail->SetFrom('febbykomputer@gmail.com', 'Toko Febby Komputer');

				$mail->Subject    = "Pemesanan Barang di Toko Febby Komputer";

				if ($jenis_pengiriman != 'COD') {
					$message = "
								<html>
								<head>
								<title>Toko Febby Komputer</title>
								</head>
								<body>
									<center>
									<h2>Konfirmasi Pembayaran</h2>
									<br><br>
									<h4>Total Pembayaran : Rp. ".$total."</h4>
									<h4>Untuk Pembayaran dapat dikirim ke rekening berikut:</h4>
									<h4>Bank BCA</h4>
									<h4>No Rekening : 8470256481</h4>
									<h4>Atas Nama : Febby Komputer</h4>
									<br>
									<p>Link untuk konfirmasi pembayaran <a href='http://localhost/penjualan/app/view/utama/konfirmasi.php?id=".$data['id_pemesanan']."'>Disini</a></p>
									</center>
								</body>
								</html>
								";
				}else{
					$message = "
								<html>
								<head>
								<title>Toko Febby Komputer</title>
								</head>
								<body>
									<center>
										<h2>Konfirmasi Pemesanan</h2>
										<h4>Kode Pemesanan</h4>
										<h3>".$data['kode_pemesanan']."</h3>
										<p>Pengambilan barang bisa di ambil langsung ke Toko dengan memperlihatkan email ini.</p> 
										<p>untuk informasi lebih lanjut bisa hubungi kontak dibawah ini.</p>
										<br>
										<h4>Alamat Toko : Jl. Ibrahim Adjie No. 47 Kiara Condong Bandung Blok CC-115 Bandung Trade Mall</h4>
										<h4>No Telepon : 082117522282</h4>
									</center>
								</body>
								</html>
								";
				}
				
				$mail->MsgHTML($message);

				$address = $data_konsumen[0]['email'];
				echo $address;
				$mail->AddAddress($address);

				if(!$mail->Send()) {
				    $_SESSION["notification_bayar"] = 'failed';
				} else {
					$_SESSION["notification_bayar"] = 'success';
				}

     		}
	    	unset($_SESSION['keranjang']);
	    	unset($_SESSION['pemesanan']);
	    	ob_end_clean();
	    	header("Location:../view/utama/pembayaran.php");
	    }
	}

	$FDaftarController = new FDaftarController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($FDaftarController, $_GET['func']));
	}

?>
