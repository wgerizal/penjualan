<?php
	include "Controller.php";
	
	class FKonfirmasiController extends Controller{

		function __construct()
	    {

			if (session_status() == PHP_SESSION_NONE) {
		        session_start();
		    }
	    	$this->pemesanan = $this->model("Pemesanan");	
	    }

	    public function index(){
	    	if (isset($_GET['id'])) {
	    		$data['pemesanan'] = $this->pemesanan->getDataPemesananById($_GET['id']);
	    	}else{
	    		$data['pemesanan'] = array();
	    	}
	     	
	    	return $data;
	    }

	    public function konfirmasi(){

	    	$target_dir = "../../uploads/bukti/";

	     	$data = $_POST;

	     	if ($_FILES['ffoto']['size'] > 0)
			{
				$file_name = basename($_FILES["ffoto"]["name"]);
				$extention = pathinfo($file_name,PATHINFO_EXTENSION);
				$filename_foto = str_replace('.', '', uniqid(rand(), true)).'.'.$extention;
			    $target_file = $target_dir . $filename_foto;
			    move_uploaded_file($_FILES["ffoto"]["tmp_name"], $target_file);
			    $data['bukti_pembayaran'] = $filename_foto;
			}

			$this->pemesanan->konfirmasi($data);
			$_SESSION["notification_konfirmasi"] = 'success';

			header("Location:../view/utama/konfirmasi.php");
	    }

	}

	$FKonfirmasiController = new FKonfirmasiController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($FKonfirmasiController, $_GET['func']));
	}
	if (isset($_POST['func']) && !empty($_POST['func'])) {
		call_user_func(array($FKonfirmasiController, $_POST['func']));
	}

?>