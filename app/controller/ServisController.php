<?php

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;	
	$current_file_path = dirname(__FILE__);
	require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '/../vendor/phpmailer/phpmailer/src/Exception.php');
	require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '/../vendor/phpmailer/phpmailer/src/PHPMailer.php');
	require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '/../vendor/phpmailer/phpmailer/src/SMTP.php');
	
	
	include "Controller.php";
	
	class ServisController extends Controller{

		private $title = 'Servis';
		private $controller = 'ServisController.php';
		public $folder = 'servis';
		public $path_controller;

		public $table_main = 'Servis';
		public $table_data = 'servis';
    	public $table_primary = 'id_servis';
    	public $table_title = array(
									'Nama Konsumen',
									'Layanan Servis',
									'Keluhan',
									'Status Servis'
								);
    	public $table_field = array(
									'nama_konsumen',
									'layanan_servis',
									'keluhan',
									'status_servis'
    							);

		public $model_main;		
    	

		function __construct()
	    {
	    	$this->auth();
	    	$this->path_controller = "../../controller/".$this->controller;
	    	$this->model_main = $this->model($this->table_main);
	    	$this->detailPemesanan = $this->model("DetailPemesanan");
	    	$this->konsumen = $this->model("Konsumen");
	    	$this->pengguna = $this->model("Pengguna");
	    }

	    public function index(){
	    	$data['title'] = $this->title;
	     	$data[$this->table_data] = $this->model_main->getDataServis();
	    	return $data;
	    }

	    public function add(){
	    	$data['title'] = $this->title;
	    	return $data;
	    }

	    public function edit(){
	    	$data['title'] = $this->title;
	    	$data[$this->table_data] = $this->model_main->getDataByID($_GET["id"]);
	    	return $data;
	    }

	    public function delete(){
	    	$message = 'Deleted data failed.';
		   	$result = 'failed';
	     	$data[$this->table_primary] = $_POST["id"];

	    	if ($this->model_main->data_delete($data)) {
	    		$message = 'Deleted data successfully.';
	    		$result = 'success';	    		
	    	}

	    	if (session_status() == PHP_SESSION_NONE) {
		        session_start();
		    }

	    	$_SESSION["notification_message"] = $message;
		    $_SESSION["notification_result"] = $result;

			echo 'true';

	    }

	    public function detail(){
	    	$data['title'] = $this->title;
	     	$data[$this->table_data] = $this->model_main->getDataServisById($_GET["id"]);
	    	return $data;
	    }

	    public function save(){

	    	$message = 'Added data failed.';
		   	$result = 'failed';
		   
		   	// form post
		   	$data = $_POST;
			
	     	if ($data['post_type'] === 'add') {
	     		if ($this->model_main->data_insert($data)) {
    				$message = 'Added data successfully.';
    				$result = 'success';		
    			}
	     	}else{

	     		if ($this->model_main->data_edit($data)) {
 					$message = 'Edit data successfully.';
    				$result = 'success';
 				}else{
		    		$message = 'Edit data failed.';
		    	}

	     	}

	     	if (session_status() == PHP_SESSION_NONE) {
		        session_start();
		    }

		    $_SESSION["notification_message"] = $message;
		    $_SESSION["notification_result"] = $result;

		    header("Location:../view/".$this->folder);

	    }

	    public function konfirmasi_servis(){

	    	$message_s = 'Edit data successfully.';
    		$result_r = 'success';
		   
		   	// form post
		   	$data = $_POST;

		   	$data_service = $this->model_main->getDataByID($data['id_servis']);
		   	$id_konsumen = $data_service[0]['id_konsumen'];
		  	$layanan_servis = $data_service[0]['layanan_servis'];
		   	$data_konsumen = $this->konsumen->getDataByID($id_konsumen);
		   	$data_pengguna = $this->pengguna->getDataByID($data_konsumen[0]['id_pengguna']);

		   	$this->model_main->konfirmasi_servis($data);
		   	ob_start();
		   	//send mail
			$mail             = new PHPMailer();

			$mail->SMTPOptions = array(
			    'ssl' => array(
			        'verify_peer' => false,
			        'verify_peer_name' => false,
			        'allow_self_signed' => true
			    )
			);
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = "mail.yourdomain.com"; // SMTP server
			$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
			                           	                // 1 = errors and messages
			                                           // 2 = messages only
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->SMTPSecure = "tls";                 
			$mail->Host       = "smtp.gmail.com";      // SMTP server
			$mail->Port       = 587;                   // SMTP port
			$mail->Username   = "febbykomputer@gmail.com";  // username
			$mail->Password   = "Rbisd$01";            // password

			$mail->SetFrom('febbykomputer@gmail.com', 'Toko Febby Komputer');

			$mail->Subject    = "Konfirmasi Servis Komputer";

			if ($layanan_servis != 'Datang Ke Toko') {
				$message = "
							<html>
							<head>
							<title>Toko Febby Komputer</title>
							</head>
							<body>
								<center>
								<h2>Konfirmasi Servis Komputer</h2>
								<br><br>
								<h4>Terimakasih Sudah Melakukan Servis Di Toko Febby Komputer</h4>  
							
									<p>Barang yang anda servis sudah selesai, pengambilan barang bisa di ambil langsung ke toko dengan memperlihatkan email ini.</p> 
									<p>untuk informasi lebih lanjut bisa hubungi kontak dibawah ini.</p>
									<br>
									<h4>Alamat Toko : Jl. Ibrahim Adjie No. 47 Kiara Condong Bandung Blok CC-115 Bandung Trade Mall</h4>
									<h4>No Telepon : 082117522282</h4>
								</center>
							</body>
							</html>
							";
			}else{
				$message = "
						
							<head>
							<title>Toko Febby Komputer</title>
							</head>
							
								<center>
									<h2>Konfirmasi Servis Komputer</h2>
									<br></br>
									<h4>Terimakasih Sudah Melakukan Servis Di Toko Febby Komputer</h4>
									
									<p>Barang yang anda servis sudah selesai, pengambilan barang bisa di ambil langsung ke toko dengan memperlihatkan email ini.</p> 
									<p>untuk informasi lebih lanjut bisa hubungi kontak dibawah ini.</p>
									<br>
									<h4>Alamat Toko : Jl. Ibrahim Adjie No. 47 Kiara Condong Bandung Blok CC-115 Bandung Trade Mall</h4>
									<h4>No Telepon : 082117522282</h4>
								</center>
							</body>
							</html>
							";
			}
			
			$mail->MsgHTML($message);
			print_r($data_pengguna);
			$address = $data_pengguna[0]['email'];
			echo $address;
			$mail->AddAddress($address);

			if(!$mail->Send()) {
			    $_SESSION["notification_bayar"] = 'failed';
			} else {
				$_SESSION["notification_bayar"] = 'success';
			}

		   	$_SESSION["notification_message"] = $message_s;
		    $_SESSION["notification_result"] = $result_r;
			ob_end_clean();	   
		    header("Location:../view/".$this->folder);
		   

	    }
	}

	$ServisController = new ServisController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($ServisController, $_GET['func']));
	}
	if (isset($_POST['func']) && !empty($_POST['func'])) {
		call_user_func(array($ServisController, $_POST['func']));
	}

?>