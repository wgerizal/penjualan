<?php
	include "Controller.php";
	
	class FDetailController extends Controller{

		function __construct()
	    {
	    	$this->barang = $this->model("Barang");	
	    	$this->kategoriBarang = $this->model("KategoriBarang");
	    }

	    public function index(){
	    	$data['barang'] = $this->barang->getDataBarangByID($_GET["id"]);
	    	return $data;
	    }

	    public function masukan(){
	    	if (session_status() == PHP_SESSION_NONE) {
			    session_start();
			}

			if (empty($_SESSION['keranjang'])) {
				$_SESSION['keranjang'] = array();
				array_push($_SESSION['keranjang'],$_POST['id_barang']);
			}else{
				array_push($_SESSION['keranjang'],$_POST['id_barang']);
			}

			header("Location:../view/utama/keranjang.php");
	    	
	    }

	}

	$FDetailController = new FDetailController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($FDetailController, $_GET['func']));
	}

?>