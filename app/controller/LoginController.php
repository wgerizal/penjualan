<?php

	include "Controller.php";



	class LoginController extends Controller{



		function __construct()

	    {

	    	if (session_status() == PHP_SESSION_NONE) {

		        session_start();

		    }

	    	$this->pengguna = $this->model('Pengguna');

	    }



	    public function login(){

	     	$email = $_POST['email'];

	     	$password = md5(trim($_POST['password']));

	     	$return = '0';

	     	if ($this->pengguna->login($email,$password)) {



	     		if ($_SESSION["login_level"] == 'konsumen') {

	     			if (!empty($_SESSION['pemesanan'])) {

			    		$_SESSION['id_konsumen'] = $_SESSION['login_id_konsumen'];

				    	$return = '3';

				    }else{

				    	$return = '2';

				    }

	     		}else{

			    	$return = '1';

			    }

				

	     	}



	     	echo $return;

	    }



	    public function logout(){

	     	if (session_status() == PHP_SESSION_NONE) {

			    session_start();

			}

		    session_unset();

		    session_destroy();

		    session_write_close();

		    setcookie(session_name(),'',0,'/');

		    session_regenerate_id(true);

	     	header("Location:../../app/view/utama/index.php");

	    }



	}



	$LoginController = new LoginController();

	call_user_func(array($LoginController, $_GET['func']));





?>