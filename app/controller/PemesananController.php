<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;	
	$current_file_path = dirname(__FILE__);
	require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '/../vendor/phpmailer/phpmailer/src/Exception.php');
	require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '/../vendor/phpmailer/phpmailer/src/PHPMailer.php');
	require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '/../vendor/phpmailer/phpmailer/src/SMTP.php');
	
	
	include "Controller.php";
	
	class PemesananController extends Controller{

		private $title = 'Pemesanan';
		private $controller = 'PemesananController.php';
		public $folder = 'pemesanan';
		public $path_controller;

		public $table_main = 'Pemesanan';
		public $table_data = 'pemesanan';
    	public $table_primary = 'id_pemesanan';
    	public $table_title = array(
									'Nama Konsumen',
									'Kode Pemesanan',
									'Jenis Pengiriman',
									'Status Pengiriman',
									'Status Pembayaran',
								);
    	public $table_field = array(
									'nama_konsumen',
									'kode_pemesanan',
									'jenis_pengiriman',
									'status_pengiriman',
									'status_pembayaran',
    							);

		public $model_main;		
    	

		function __construct()
	    {
	    	$this->auth();
	    	$this->path_controller = "../../controller/".$this->controller;
	    	$this->model_main = $this->model($this->table_main);
	    	$this->detailPemesanan = $this->model("DetailPemesanan");
	    	$this->konsumen = $this->model("Konsumen");
	    }

	    public function index(){
	    	$data['title'] = $this->title;
	     	$data[$this->table_data] = $this->model_main->getDataPemesanan();
	    	return $data;
	    }

	    public function add(){
	    	$data['title'] = $this->title;
	    	return $data;
	    }

	    public function edit(){
	    	$data['title'] = $this->title;
	    	$data[$this->table_data] = $this->model_main->getDataByID($_GET["id"]);
	    	return $data;
	    }

	    public function delete(){
	    	$message = 'Deleted data failed.';
		   	$result = 'failed';
	     	$data[$this->table_primary] = $_POST["id"];

	    	if ($this->model_main->data_delete($data)) {
	    		$message = 'Deleted data successfully.';
	    		$result = 'success';	    		
	    	}

	    	if (session_status() == PHP_SESSION_NONE) {
		        session_start();
		    }

	    	$_SESSION["notification_message"] = $message;
		    $_SESSION["notification_result"] = $result;

			echo 'true';

	    }

	    public function detail(){
	    	$data['title'] = $this->title;
	     	$data[$this->table_data] = $this->model_main->getDataPemesananById($_GET["id"]);
	     	$data["detailPemesanan"] = $this->detailPemesanan->getDataDetailPemesanan($_GET["id"]);
	    	return $data;
	    }

	    public function save(){

	    	$message = 'Added data failed.';
		   	$result = 'failed';
		   
		   	// form post
		   	$data = $_POST;
			
	     	if ($data['post_type'] === 'add') {
	     		if ($this->model_main->data_insert($data)) {
    				$message = 'Added data successfully.';
    				$result = 'success';		
    			}
	     	}else{

	     		if ($this->model_main->data_edit($data)) {
 					$message = 'Edit data successfully.';
    				$result = 'success';
 				}else{
		    		$message = 'Edit data failed.';
		    	}

	     	}

	     	if (session_status() == PHP_SESSION_NONE) {
		        session_start();
		    }

		    $_SESSION["notification_message"] = $message;
		    $_SESSION["notification_result"] = $result;

		    header("Location:../view/".$this->folder);

	    }

	    public function konfirmasi_pembayaran(){

	    	$message = 'Edit data successfully.';
    		$result = 'success';
		   
		   	// form post
		   	$data = $_POST;

		   	$this->model_main->konfirmasi_pembayaran($data);

		   	$pemesanan = $this->model_main->getDataPemesananById($data['id_pemesanan']);
		   	$konsumen = $this->konsumen->getDataKonsumenById($pemesanan[0]['id_konsumen']);
		   	//send mail
		   	ob_start();
			$mail             = new PHPMailer();

			$mail->SMTPOptions = array(
			    'ssl' => array(
			        'verify_peer' => false,
			        'verify_peer_name' => false,
			        'allow_self_signed' => true
			    )
			);
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = "mail.yourdomain.com"; // SMTP server
			$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
			                                           // 1 = errors and messages
			                                           // 2 = messages only
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->SMTPSecure = "tls";                 
			$mail->Host       = "smtp.gmail.com";      // SMTP server
			$mail->Port       = 587;                   // SMTP port
			$mail->Username   = "febbykomputer@gmail.com";  // username
			$mail->Password   = "Rbisd$01";            // password

			$mail->SetFrom('febbykomputer@gmail.com', 'Toko Febby Komputer');

			$mail->Subject    = "Pemesanan Barang di Toko Febby Komputer";

			$message = "<html>
						<head>
						<title>Toko Febby Komputer</title>
						</head>
						<body>
							<center>
							<h2>Konfirmasi Pembayaran Berhasil</h2>
							<br><br>
							<h4>Nama Pemesan : ".$pemesanan[0]['nama_konsumen']."</h4>
							<h4>Kode Pemesanan : ".$pemesanan[0]['kode_pemesanan']."</h4>
							<h4>Total Pembayaran : Rp. ".$pemesanan[0]['total_bayar']."</h4>
							<br>
							<p>Terima kasih atas kepercayaanya telah membeli produk di toko kami.</p>
							</center>
						</body>
						</html>
						";
			
			$mail->MsgHTML($message);

			$address = $konsumen[0]['email'];
			$mail->AddAddress($address);

			if(!$mail->Send()) {
			    $message = 'Edit data failed.';
		   		$result = 'failed';
			} else {
				$message = 'Edit data successfully.';
				$result = 'success';
			}

		   	$_SESSION["notification_message"] = $message;
		    $_SESSION["notification_result"] = $result;
		    ob_end_clean();
		    header("Location:../view/".$this->folder);
		    

	    }
	     public function pembayaran_gagal(){

	    	$message = 'Edit data successfully.';
    		$result = 'success';
		   
		   	// form post
		   	$data = $_POST;


		   	$pemesanan = $this->model_main->getDataPemesananById($data['id_pemesanan']);
		   	$konsumen = $this->konsumen->getDataKonsumenById($pemesanan[0]['id_konsumen']);
		   	//send mail
		   	ob_start();
			$mail             = new PHPMailer();

			$mail->SMTPOptions = array(
			    'ssl' => array(
			        'verify_peer' => false,
			        'verify_peer_name' => false,
			        'allow_self_signed' => true
			    )
			);
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = "mail.yourdomain.com"; // SMTP server
			$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
			                                           // 1 = errors and messages
			                                           // 2 = messages only
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->SMTPSecure = "tls";                 
			$mail->Host       = "smtp.gmail.com";      // SMTP server
			$mail->Port       = 587;                   // SMTP port
			$mail->Username   = "febbykomputer@gmail.com";  // username
			$mail->Password   = "Rbisd$01";            // password

			$mail->SetFrom('febbykomputer@gmail.com', 'Toko Febby Komputer');

			$mail->Subject    = "Pembayaran Gagal";

			$message = "<html>
						<head>
						<title>Toko Febby Komputer</title>
						</head>
						<body>
							<center>
							<h2>Pembayaran Gagal</h2>
							<br><br>
							<h4>Nama Pemesan : ".$pemesanan[0]['nama_konsumen']."</h4>
							<h4>Kode Pemesanan : ".$pemesanan[0]['kode_pemesanan']."</h4>
							<h4>Total Pembayaran : Rp. ".$pemesanan[0]['total_bayar']."</h4>
							<br>
							<p>Silahkan upload ulang bukti bayar.</p>
							<p>Link untuk konfirmasi pembayaran <a href='http://febbykomputer.com/penjualan/app/view/utama/konfirmasi.php?id=".$data['id_pemesanan']."'>Disini</a></p>
							</center>
						</body>
						</html>
						";
			
			$mail->MsgHTML($message);

			$address = $konsumen[0]['email'];
			$mail->AddAddress($address);

			if(!$mail->Send()) {
			    $message = 'Edit data failed.';
		   		$result = 'failed';
			} else {
				$message = 'Edit data successfully.';
				$result = 'success';
			}

		   	$_SESSION["notification_message"] = $message;
		    $_SESSION["notification_result"] = $result;
		    ob_end_clean();
		    header("Location:../view/".$this->folder);
		    

	    }

	    public function konfirmasi_pengiriman(){

	    	$message = 'Edit data successfully.';
    		$result = 'success';
		   
		   	// form post
		   	$data = $_POST;

		   	$this->model_main->konfirmasi_pengiriman($data);

		   	$_SESSION["notification_message"] = $message;
		    $_SESSION["notification_result"] = $result;

		    header("Location:../view/".$this->folder);

	    }
	}

	$PemesananController = new PemesananController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($PemesananController, $_GET['func']));
	}
	if (isset($_POST['func']) && !empty($_POST['func'])) {
		call_user_func(array($PemesananController, $_POST['func']));
	}

?>