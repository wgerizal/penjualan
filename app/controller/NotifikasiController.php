<?php
	include "Controller.php";
	
	class NotifikasiController extends Controller{

		private $controller = 'NotifikasiController.php';

		function __construct()
	    {
	    	$this->auth();
	    	$this->notifikasi = $this->model('Notifikasi');
	    }

	    public function index(){
	    }

	    public function go(){
	    	$id = $_GET['id'];
	    	$data = $this->notifikasi->getDataByID($id); 
	    	if ($this->notifikasi->data_delete($data[0])) {
	    		header("Location:../view/".$data[0]['aksi']);
	    	}
	    }
	}

	$NotifikasiController = new NotifikasiController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($NotifikasiController, $_GET['func']));
	}
	if (isset($_POST['func']) && !empty($_POST['func'])) {
		call_user_func(array($NotifikasiController, $_POST['func']));
	}

?>