<?php

	/**
	* 
	*/
	class Controller
	{

		function __construct()
		{
		}

		public static function model($className) {

			if (file_exists(dirname(__FILE__)."/../model/".$className . '.php')) { 
		        include dirname(__FILE__)."/../model/".$className . '.php'; 
		        $model = new $className();
		        return $model; 
		    } 
		    return false; 
	    }

	    public static function auth(){
	    	if (session_status() == PHP_SESSION_NONE) {
			    session_start();
			}
			if (!isset($_SESSION['login_id']) && empty($_SESSION['login_id'])) {
				if (file_exists("../../../index.php")) { 
			        header("Location: ../../../index.php");
			    }
				
			}
	    }

	}

?>