<?php

	include "Controller.php";

	

	class BarangController extends Controller{



		private $title = 'Barang';

		private $controller = 'BarangController.php';

		public $folder = 'barang';

		public $path_controller;



		public $table_main = 'Barang';

		public $table_data = 'barang';

    	public $table_primary = 'id_barang';

    	public $table_title = array(

									'Nama Supplier',

									'Kategori Barang',

									'Nama Barang',

									'Harga Barang',

									'Keterangan Barang',

									'Gambar'

								);

    	public $table_field = array(

    								'nama_supplier',

									'nama_kategori',

									'nama_barang',

									'harga_barang',

									'keterangan_barang',

									'gambar'

    							);



		public $model_main;		

    	



		function __construct()

	    {

	    	$this->auth();

	    	$this->path_controller = "../../controller/".$this->controller;

	    	$this->model_main = $this->model($this->table_main);

	    	$this->supplier = $this->model("Supplier");

	    	$this->kategoriBarang = $this->model("KategoriBarang");

	    	$this->stokBarang = $this->model("StokBarang");

	    }



	    public function index(){

	    	$data['title'] = $this->title;

	     	$data[$this->table_data] = $this->model_main->getDataBarang();

	    	return $data;

	    }



	    public function add(){

	    	$data['title'] = $this->title;

	    	$data['supplier'] = $this->supplier->getDataAll();

	    	$data['kategoriBarang'] = $this->kategoriBarang->getDataAll();

	    	return $data;

	    }



	    public function edit(){

	    	$data['title'] = $this->title;

	    	$data[$this->table_data] = $this->model_main->getDataBarangByID($_GET["id"]);

	    	$data['supplier'] = $this->supplier->getDataAll();

	    	$data['kategoriBarang'] = $this->kategoriBarang->getDataAll();

	    	return $data;

	    }



	    public function delete(){

	    	$message = 'Deleted data failed.';

		   	$result = 'failed';

	     	$data[$this->table_primary] = $_POST["id"];



	    	if ($this->model_main->data_delete($data)) {

	    		$message = 'Deleted data successfully.';

	    		$result = 'success';	    		

	    	}



	    	if (session_status() == PHP_SESSION_NONE) {

		        session_start();

		    }



	    	$_SESSION["notification_message"] = $message;

		    $_SESSION["notification_result"] = $result;



			echo 'true';



	    }



	    public function detail(){

	    	$data['title'] = $this->title;

	     	$data[$this->table_data] = $this->model_main->getDataAll($_GET["id"]);

	    	return $data;

	    }



	    public function save(){



	    	$message = 'Added data failed.';

		   	$result = 'failed';

		   	$target_dir = "../../uploads/image/";

		   

		   	// form post

		   	$data = $_POST;

			

			if (isset($data['foto_ext'])) {

				$data['gambar'] = $data['foto'].'.'.$data['foto_ext'];

			}



		   	$check = getimagesize($_FILES["ffoto"]["tmp_name"]);

			if ($check !== false)

			{

				$file_name = basename($_FILES["ffoto"]["name"]);

				$extention = pathinfo($file_name,PATHINFO_EXTENSION);

				$filename_foto = str_replace('.', '', uniqid(rand(), true)).'.'.$extention;

			    $target_file = $target_dir . $filename_foto;

			    move_uploaded_file($_FILES["ffoto"]["tmp_name"], $target_file);

			    $data['gambar'] = $filename_foto;

			}



	     	if ($data['post_type'] === 'add') {

	     		$data['jumlah'] = 0;

	     		$data['id_stok_barang'] = $this->stokBarang->data_insert($data, true);

	     		if ($data['id_stok_barang'] != 0) {

	     			if ($this->model_main->data_insert($data)) {

	    				$message = 'Added data successfully.';

	    				$result = 'success';		

	    			}

	     		}

	     		

	     	}else{



	     		if ($this->model_main->data_edit($data)) {

 					$message = 'Edit data successfully.';

    				$result = 'success';

 				}else{

		    		$message = 'Edit data failed.';

		    	}



	     	}



	     	if (session_status() == PHP_SESSION_NONE) {

		        session_start();

		    }



		    $_SESSION["notification_message"] = $message;

		    $_SESSION["notification_result"] = $result;



		    header("Location:../view/".$this->folder);



	    }

	}



	$BarangController = new BarangController();

	if (isset($_GET['func']) && !empty($_GET['func'])) {

		call_user_func(array($BarangController, $_GET['func']));

	}

	if (isset($_POST['func']) && !empty($_POST['func'])) {

		call_user_func(array($BarangController, $_POST['func']));

	}



?>