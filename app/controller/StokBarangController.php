<?php

	include "Controller.php";

	

	class StokBarangController extends Controller{



		private $title = 'Stok Barang';

		private $controller = 'StokBarangController.php';

		public $folder = 'stok-barang';

		public $path_controller;



		public $table_main = 'StokBarang';

		public $table_data = 'stokBarang';

    	public $table_primary = 'id_stok_barang';

    	public $table_title = array(

    								'Nama Barang',

									'Jumlah',

								);

    	public $table_field = array(

    								'nama_barang',

									'jumlah',

    							);



		public $model_main;		

    	



		function __construct()

	    {

	    	$this->auth();

	    	$this->path_controller = "../../controller/".$this->controller;

	    	$this->model_main = $this->model($this->table_main);
	    	$this->logStok = $this->model("LogStok");
	    	$this->barang = $this->model("Barang");

	    }



	    public function index(){

	    	$data['title'] = $this->title;

	     	$data[$this->table_data] = $this->model_main->getDataStokBarang();

	    	return $data;

	    }



	    public function add(){

	    	$data['title'] = $this->title;

	    	return $data;

	    }



	    public function edit(){

	    	$data['title'] = $this->title;

	    	$data[$this->table_data] = $this->model_main->getDataStokBarangById($_GET["id"]);

	    	return $data;

	    }



	    public function delete(){

	    	$message = 'Deleted data failed.';

		   	$result = 'failed';

	     	$data[$this->table_primary] = $_POST["id"];



	    	if ($this->model_main->data_delete($data)) {

	    		$message = 'Deleted data successfully.';

	    		$result = 'success';	    		

	    	}



	    	if (session_status() == PHP_SESSION_NONE) {

		        session_start();

		    }



	    	$_SESSION["notification_message"] = $message;

		    $_SESSION["notification_result"] = $result;



			echo 'true';



	    }



	    public function detail(){

	    	$data['title'] = $this->title;

	     	$data[$this->table_data] = $this->model_main->getDataAll($_GET["id"]);

	    	return $data;

	    }



	    public function save(){



	    	$message = 'Added data failed.';

		   	$result = 'failed';

		   

		   	// form post

		   	$data = $_POST;

			

	     	if ($data['post_type'] === 'add') {

	     		if ($this->model_main->data_insert($data)) {

    				$message = 'Added data successfully.';

    				$result = 'success';		

    			}

	     	}else{
	     		$data_barang= $this->barang->getDataBarangById($data['id_barang']);
	     		$stok_awal = $data_barang[0]['jumlah'];
	     		$update_stok = $data['jumlah'];
	     		if ($this->model_main->data_edit($data)) {
	     			$data['status_stok'] = 'masuk';

	     			
 					$message = 'Edit data successfully.';

    				$result = 'success';

 				}else{

		    		$message = 'Edit data failed.';

		    	}
	     		if($stok_awal > $update_stok){
	     			$data['jumlah'] = abs($stok_awal - $update_stok);
	     			$data['status_stok'] = 'keluar';
	     			$this->logStok->data_insert($data);
	     		}else if($stok_awal < $update_stok){
	     			$data['jumlah'] = abs($stok_awal - $update_stok);
	     			$data['status_stok'] = 'masuk';
	     			$this->logStok->data_insert($data);
	     		} 
	     		



	     	}



	     	if (session_status() == PHP_SESSION_NONE) {

		        session_start();

		    }



		    $_SESSION["notification_message"] = $message;

		    $_SESSION["notification_result"] = $result;



		    header("Location:../view/".$this->folder);



	    }

	}



	$StokBarangController = new StokBarangController();

	if (isset($_GET['func']) && !empty($_GET['func'])) {

		call_user_func(array($StokBarangController, $_GET['func']));

	}

	if (isset($_POST['func']) && !empty($_POST['func'])) {

		call_user_func(array($StokBarangController, $_POST['func']));

	}



?>