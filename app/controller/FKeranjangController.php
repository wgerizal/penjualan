<?php
	include "Controller.php";
	
	class FKeranjangController extends Controller{

		function __construct()
	    {
	    	if (session_status() == PHP_SESSION_NONE) {
			    session_start();
			}
			$this->barang = $this->model("Barang");	
	    }

	    public function index(){
	    	if (!empty($_SESSION["keranjang"])) {
	    		$data['keranjang'] = $this->barang->getDataBarangByIdArray($_SESSION["keranjang"]);
	    	}else{
	    		$data['keranjang'] = array();
	    	}
	     	

	    	return $data;
	    }

	    public function delete(){

	    	$id_barang = $_GET['id_barang'];
	    	$list = $_SESSION['keranjang'];
	    	unset($_SESSION['keranjang']);
	    	foreach ($list as $key => $value) {
	    		if ($value == $id_barang) {
	    			unset($list[$key]);
	    		}
	    	}

	    	$_SESSION['keranjang'] = array();
	    	$_SESSION['keranjang'] = $list;

	    	header("Location:../view/utama/keranjang.php");
	    }

	    public function kosongin(){
	    	unset($_SESSION['keranjang']);
	    	header("Location:../view/utama/keranjang.php");
	    }

	    public function lanjut(){
	    	$_SESSION['pemesanan'] = array();
	    	$pemesanan = array('id_barang' => $_POST['barang_id'], 'qty' => $_POST['qty'], 'jenis_pengiriman' => $_POST['jenis_pengiriman'] );
	    	$_SESSION['pemesanan'] = $pemesanan;
	    	if (isset($_SESSION['login_id']) && !empty($_SESSION['login_id'])) {
	    		if (!empty($_SESSION['pemesanan'])) {
	    			$_SESSION['id_konsumen'] = $_SESSION['login_id_konsumen'];
			    	header("Location:../view/utama/pembayaran.php");
			    }else{
			    	header("Location:../view/utama/index.php");
			    }
	    	}else{
	    		header("Location:../view/utama/daftar.php");	
	    	}

	    	
	    }


	}

	$FKeranjangController = new FKeranjangController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($FKeranjangController, $_GET['func']));
	}

?>