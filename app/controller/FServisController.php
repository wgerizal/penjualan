<?php
	include "Controller.php";
	
	class FServisController extends Controller{

		function __construct()
	    {
	    	if (session_status() == PHP_SESSION_NONE) {
		        session_start();
		    }
	    	$this->barang = $this->model("Barang");	
	    	$this->servis = $this->model("Servis");	
	    	$this->konsumen = $this->model("Konsumen");	
	    	$this->kategoriBarang = $this->model("KategoriBarang");
	    }

	    public function index(){
	    	$data['kategoriBarang'] = $this->kategoriBarang->getDataAll();
	    	return $data;
	    }

	    public function servis(){

	    	if (isset($_SESSION['login_id_konsumen']) && !empty($_SESSION['login_id_konsumen'])) {
	    		$data = $_POST;
		    	$data['id_konsumen'] = $_SESSION['login_id_konsumen'];
		    	$data['status_servis'] = 'Belum Selesai';
		    	$data['tanggal'] = date('Y-m-d');

		    	$this->servis->data_insert($data);

		    	$data_konsumen = $this->konsumen->getDataKonsumenById($_SESSION['login_id_konsumen']);
		    	
		    	$_SESSION["notification_servis"] = 'success';

				header("Location:../view/utama/servis.php");
	    	}else{
	    		$_SESSION['servis'] = array();
	    		$servis = array('id_ketegori_barang' => $_POST['id_ketegori_barang'], 'layanan_servis' => $_POST['layanan_servis'], 'keluhan' => $_POST['keluhan'] );
	    		$_SESSION['servis'] = $servis;
	    		header("Location:../view/utama/daftar.php");	
	    	}

	    	
	    	
	    }

	}

	$FServisController = new FServisController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($FServisController, $_GET['func']));
	}

?>
