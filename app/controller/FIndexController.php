<?php
	include "Controller.php";
	
	class FIndexController extends Controller{

		function __construct()
	    {
	    	$this->barang = $this->model("Barang");	
	    	$this->kategoriBarang = $this->model("KategoriBarang");
	    }

	    public function index(){
	     	$data['barang'] = $this->barang->getDataBarang();
	     	$data['kategoriBarang'] = $this->kategoriBarang->getDataAll();
	    	return $data;
	    }

	}

	$FIndexController = new FIndexController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($FIndexController, $_GET['func']));
	}

?>