<?php
	include "Controller.php";
	
	class DashboardController extends Controller{

		function __construct()
	    {
	    	$this->auth();
	    	$this->notifikasi = $this->model('Notifikasi');
	    }

	    public function index(){
	     	$data['title'] = 'Dashboard';
	    	$data['total_calon_karyawan'] = 5;
	    	$data['notifikasi'] = $this->notifikasi->getDataAll();
	    	return $data;
	    }

	}

	$DashboardController = new DashboardController();
	if (isset($_GET['func']) && !empty($_GET['func'])) {
		call_user_func(array($DashboardController, $_GET['func']));
	}

?>