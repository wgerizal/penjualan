<?php
	
	include "Database.php";

	class Servis extends Database{

		private $table = "servis";
		private $primary = "id_servis";
		private $field = array(
									'id_servis',
									'id_konsumen',
									'layanan_servis',
									'keluhan',
									'status_servis',
									'tanggal'
							  );
		private $field_update = array(
										'status_servis'
									 );

		function __construct()
	    {
	    		
	    }

	    public function getDataAll(){
	    	$result = $this->select('*',$this->table);
	     	return $result;
	    }

	    public function getDataByID($id){
	    	$result = $this->selectWhere('*',$this->table,$this->primary." = '$id'");
	     	return $result;
	    }

	    public function data_insert($data, $id_insert = false){
	    	//string field insert
	    	$field = "";
	    	$i = 0;
			$len = count($this->field);
	    	foreach ($this->field as $column) {
	    		$field = $field."`".$column."`";
	    		if ($i != ($len - 1)) {
	    			$field = $field.", ";
	    		}
	    		$i++;
	    	}

	    	//string value insert
	    	$value = "NULL, ";
	    	$i = 0;
	    	foreach ($this->field as $column) {
	    		if ($this->primary != $column) {
	    			$value = $value."'".$data[$column]."'";

	    			if ($i != ($len - 1)) {
		    			$value = $value.", ";
		    		}
	    		}
	    		
	    		$i++;
	    	}

	    	//insert
	    	if ($id_insert) {
	    		$result = 0;
	    		$id_insert_data = $this->insert($this->table,$field,$value,$id_insert);
		    	if ($id_insert_data != 0) {
		    		$result = $id_insert_data;
		    	}
	    	}else{
	    		$result = false;
		    	if ($this->insert($this->table,$field,$value,$id_insert)) {
		    		$result = true;
		    	}	
	    	}

	     	return $result;
	    }

	    public function data_edit($data){
	    	$where = $this->primary." = ".$data[$this->primary];

	    	// value edit
	    	$value = "";
	    	$i = 0;
			$len = count($this->field_update);
	    	foreach ($this->field_update as $column) {
	    		$value = $value."`".$column."` = '".$data[$column]."'";
	    		if ($i != ($len - 1)) {
	    			$value = $value.", ";
	    		}
	    		$i++;
	    	}

	    	$result = false;

	    	if ($this->update($this->table,$value,$where)) {
	    		$result = true;
	    	}
	    	
	     	return $result;
	    }


	    public function data_delete($data){
	    	$where = $this->primary." = ".$data[$this->primary];
	    	$result = false;

	    	if ($this->delete($this->table,$where)) {
	    		$result = true;
	    	}
	    	
	     	return $result;
	    }


	    // custom

	    public function getDataServis(){
	    	$result = $this->raw("SELECT * FROM `servis` a JOIN `konsumen` b on a.`id_konsumen` = b.`id_konsumen`");
	     	return $result;
	    }

	    public function getDataServisById($id){
	    	$result = $this->raw("SELECT * FROM `servis` a JOIN `konsumen` b on a.`id_konsumen` = b.`id_konsumen` where a.`id_servis` = ".$id);
	     	return $result;
	    }

	    public function konfirmasi_servis($data){
	    	$result = $this->raw("UPDATE `servis` SET `status_servis` = 'Sudah Selesai' WHERE `id_servis` = ".$data['id_servis']);
	     	return $result;
	    }

	    public function getDataServisLaporan($data){
	    	$result = $this->raw("SELECT * FROM `servis` a JOIN `konsumen` b on a.`id_konsumen` = b.`id_konsumen` WHERE `tanggal` BETWEEN '".$data['fdate']."' AND '".$data['tdate']."'");
	     	return $result;
	    }

	    
	}

?>