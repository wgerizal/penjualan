<?php

	

	include "Database.php";



	class LogStok extends Database{



		private $table = "log_stok";

		private $primary = "id_log";

		private $field = array(

									'id_log',

									'id_barang',

									'status_stok',

									'jumlah'


							  );

		private $field_update = array(
										'status_stok',

										'jumlah',

									 );



		function __construct()

	    {

	    		

	    }



	    public function getDataAll(){

	    	$result = $this->select('*',$this->table);

	     	return $result;

	    }



	    public function getDataByID($id){

	    	$result = $this->selectWhere('*',$this->table,$this->primary." = '$id'");

	     	return $result;

	    }



	    public function data_insert($data, $id_insert = false){

	    	//string field insert

	    	$field = "";

	    	$i = 0;

			$len = count($this->field);

	    	foreach ($this->field as $column) {

	    		$field = $field."`".$column."`";

	    		if ($i != ($len - 1)) {

	    			$field = $field.", ";

	    		}

	    		$i++;

	    	}



	    	//string value insert

	    	$value = "NULL, ";

	    	$i = 0;

	    	foreach ($this->field as $column) {

	    		if ($this->primary != $column) {

	    			$value = $value."'".$data[$column]."'";



	    			if ($i != ($len - 1)) {

		    			$value = $value.", ";

		    		}

	    		}

	    		

	    		$i++;

	    	}



	    	//insert

	    	if ($id_insert) {

	    		$result = 0;

	    		$id_insert_data = $this->insert($this->table,$field,$value,$id_insert);

		    	if ($id_insert_data != 0) {

		    		$result = $id_insert_data;

		    	}

	    	}else{

	    		$result = false;

		    	if ($this->insert($this->table,$field,$value,$id_insert)) {

		    		$result = true;

		    	}	

	    	}



	     	return $result;

	    }



	    public function data_edit($data){

	    	$where = $this->primary." = ".$data[$this->primary];



	    	// value edit

	    	$value = "";

	    	$i = 0;

			$len = count($this->field_update);

	    	foreach ($this->field_update as $column) {

	    		$value = $value."`".$column."` = '".$data[$column]."'";

	    		if ($i != ($len - 1)) {

	    			$value = $value.", ";

	    		}

	    		$i++;

	    	}



	    	$result = false;



	    	if ($this->update($this->table,$value,$where)) {

	    		$result = true;

	    	}

	    	

	     	return $result;

	    }





	    public function data_delete($data){

	    	$where = $this->primary." = ".$data[$this->primary];

	    	$result = false;



	    	if ($this->delete($this->table,$where)) {

	    		$result = true;

	    	}

	    	

	     	return $result;

	    }





	    // custom



	    public function getDataLaporanStok($data){

	    	$result = $this->raw("SELECT status_stok, nama_barang, nama_kategori, harga_barang, jumlah, tanggal as tanggal FROM `log_stok` a JOIN `barang` b on a.`id_barang` = b.`id_barang`
	    		JOIN `kategori_barang` c on b.`id_kategori_barang` = b.`id_kategori_barang`
	    		WHERE DATE(tanggal) BETWEEN '".$data['fdate']."' AND '".$data['tdate']."' AND b.`id_barang` = '".$data['produk']."'
	    		GROUP BY id_log");

	     	return $result;

	    }



	}



?>