<?php
	
	include "Database.php";

	class Pengguna extends Database{

		private $table = "pengguna";
		private $primary = "id_pengguna";
		private $field = array(
									'id_pengguna',
									'username',
									'email',
									'password',
									'level_pengguna',
									'status_pengguna'
							  );
		private $field_update = array(
										'username',
										'email',
										'password',
										'level_pengguna',
										'status_pengguna'
									 );

		function __construct()
	    {
	    		
	    }

	    public function getDataAll(){
	    	$result = $this->select('*',$this->table);
	     	return $result;
	    }

	    public function getDataByID($id){
	    	$result = $this->selectWhere('*',$this->table,$this->primary." = '$id'");
	     	return $result;
	    }

	    public function data_insert($data, $id_insert = false){
	    	//string field insert
	    	$field = "";
	    	$i = 0;
			$len = count($this->field);
	    	foreach ($this->field as $column) {
	    		$field = $field."`".$column."`";
	    		if ($i != ($len - 1)) {
	    			$field = $field.", ";
	    		}
	    		$i++;
	    	}

	    	//string value insert
	    	$value = "NULL, ";
	    	$i = 0;
	    	foreach ($this->field as $column) {
	    		if ($this->primary != $column) {
	    			$value = $value."'".$data[$column]."'";

	    			if ($i != ($len - 1)) {
		    			$value = $value.", ";
		    		}
	    		}
	    		
	    		$i++;
	    	}

	    	//insert
	    	if ($id_insert) {
	    		$result = 0;
	    		$id_insert_data = $this->insert($this->table,$field,$value,$id_insert);
		    	if ($id_insert_data != 0) {
		    		$result = $id_insert_data;
		    	}
	    	}else{
	    		$result = false;
		    	if ($this->insert($this->table,$field,$value,$id_insert)) {
		    		$result = true;
		    	}	
	    	}

	     	return $result;
	    }

	    public function data_edit($data){
	    	$where = $this->primary." = ".$data[$this->primary];

	    	// value edit
	    	$value = "";
	    	$i = 0;
			$len = count($this->field_update);
	    	foreach ($this->field_update as $column) {
	    		$value = $value."`".$column."` = '".$data[$column]."'";
	    		if ($i != ($len - 1)) {
	    			$value = $value.", ";
	    		}
	    		$i++;
	    	}

	    	$result = false;

	    	if ($this->update($this->table,$value,$where)) {
	    		$result = true;
	    	}
	    	
	     	return $result;
	    }


	    public function data_delete($data){
	    	$where = $this->primary." = ".$data[$this->primary];
	    	$result = false;

	    	if ($this->delete($this->table,$where)) {
	    		$result = true;
	    	}
	    	
	     	return $result;
	    }


	    // custom

	    public function cek_password($id_pengguna, $password){
	    	$result = $this->selectWhere('*',$this->table,"`id_pengguna` = '$id_pengguna' and `password` = '$password'");
	     	return $result;
	    }

	    public function login($email,$password){

	    	$result = false;
	    	$data = $this->selectWhere('*',$this->table,"`email` = '$email' and `password` = '$password' and `status_pengguna` = 1");

	    	if (count($data) > 0) {
	    		$result = true;
	    		if (session_status() == PHP_SESSION_NONE) {
				    session_start();
				}
				$_SESSION["login_id"] = $data[0]['id_pengguna'];
				$_SESSION["login_email"] = $data[0]['email'];
				$_SESSION["login_level"] = $data[0]['level_pengguna'];
				$_SESSION["login_nama"] = $data[0]['username'];
				if ($data[0]['level_pengguna'] == 'konsumen') {
					$data_k = $this->raw("SELECT * FROM `konsumen` where `id_pengguna` = ".$data[0]['id_pengguna']);
					$_SESSION["login_id_konsumen"] = $data_k[0]['id_konsumen'];
				}
				
	    	}

	     	return $result;
	    }

	}

?>