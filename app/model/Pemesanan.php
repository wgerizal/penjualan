<?php
	
	include "Database.php";

	class Pemesanan extends Database{

		private $table = "pemesanan";
		private $primary = "id_pemesanan";
		private $field = array(
									'id_pemesanan',
									'id_konsumen',
									'kode_pemesanan',
									'total_bayar',
									'jenis_pengiriman',
									'status_pengiriman',
									'status_pembayaran',
									'bukti_pembayaran',
									'tanggal',
							  );
		private $field_update = array(
										'status_pengiriman',
										'status_pembayaran',
										'bukti_pembayaran',
									 );

		function __construct()
	    {
	    		
	    }

	    public function getDataAll(){
	    	$result = $this->select('*',$this->table);
	     	return $result;
	    }

	    public function getDataByID($id){
	    	$result = $this->selectWhere('*',$this->table,$this->primary." = '$id'");
	     	return $result;
	    }

	    public function data_insert($data, $id_insert = false){
	    	//string field insert
	    	$field = "";
	    	$i = 0;
			$len = count($this->field);
	    	foreach ($this->field as $column) {
	    		$field = $field."`".$column."`";
	    		if ($i != ($len - 1)) {
	    			$field = $field.", ";
	    		}
	    		$i++;
	    	}

	    	//string value insert
	    	$value = "NULL, ";
	    	$i = 0;
	    	foreach ($this->field as $column) {
	    		if ($this->primary != $column) {
	    			$value = $value."'".$data[$column]."'";

	    			if ($i != ($len - 1)) {
		    			$value = $value.", ";
		    		}
	    		}
	    		
	    		$i++;
	    	}

	    	//insert
	    	if ($id_insert) {
	    		$result = 0;
	    		$id_insert_data = $this->insert($this->table,$field,$value,$id_insert);
		    	if ($id_insert_data != 0) {
		    		$result = $id_insert_data;
		    	}
	    	}else{
	    		$result = false;
		    	if ($this->insert($this->table,$field,$value,$id_insert)) {
		    		$result = true;
		    	}	
	    	}

	     	return $result;
	    }

	    public function data_edit($data){
	    	$where = $this->primary." = ".$data[$this->primary];

	    	// value edit
	    	$value = "";
	    	$i = 0;
			$len = count($this->field_update);
	    	foreach ($this->field_update as $column) {
	    		$value = $value."`".$column."` = '".$data[$column]."'";
	    		if ($i != ($len - 1)) {
	    			$value = $value.", ";
	    		}
	    		$i++;
	    	}

	    	$result = false;

	    	if ($this->update($this->table,$value,$where)) {
	    		$result = true;
	    	}
	    	
	     	return $result;
	    }


	    public function data_delete($data){
	    	$where = $this->primary." = ".$data[$this->primary];
	    	$result = false;

	    	if ($this->delete($this->table,$where)) {
	    		$result = true;
	    	}
	    	
	     	return $result;
	    }


	    // custom

	    public function getDataPemesanan(){
	    	$result = $this->raw("SELECT * FROM `pemesanan` a JOIN `konsumen` b on a.`id_konsumen` = b.`id_konsumen`");
	     	return $result;
	    }

	    public function getDataPemesananById($id){
	    	$result = $this->raw("SELECT * FROM `pemesanan` a JOIN `konsumen` b on a.`id_konsumen` = b.`id_konsumen` where a.`id_pemesanan` = ".$id);
	     	return $result;
	    }

	    public function updateTotal($id_pemesanan,$total){
	    	$result = $this->raw("UPDATE `pemesanan` SET `total_bayar` = $total WHERE `id_pemesanan` = ".$id_pemesanan);
	     	return $result;
	    }

	    public function konfirmasi($data){
	    	$result = $this->raw("UPDATE `pemesanan` SET `bukti_pembayaran` = '".$data['bukti_pembayaran']."' WHERE `id_pemesanan` = ".$data['id_pemesanan']);
	     	return $result;
	    }

	    public function konfirmasi_pembayaran($data){
	    	$result = $this->raw("UPDATE `pemesanan` SET `status_pembayaran` = 'Lunas' WHERE `id_pemesanan` = ".$data['id_pemesanan']);
	     	return $result;
	    }

	    public function konfirmasi_pengiriman($data){
	    	$result = $this->raw("UPDATE `pemesanan` SET `status_pengiriman` = 'Sudah Dikirim' WHERE `id_pemesanan` = ".$data['id_pemesanan']);
	     	return $result;
	    }

	    public function getDataPemesananLaporan($data){
	    	// SELECT * FROM `pemesanan` a JOIN `konsumen` b on a.`id_konsumen` = b.`id_konsumen` WHERE tanggal BETWEEN '2018-01-07' AND '2018-01-13'
	    	$result = $this->raw("SELECT * FROM `pemesanan` a JOIN `konsumen` b on a.`id_konsumen` = b.`id_konsumen` WHERE `tanggal` BETWEEN '".$data['fdate']."' AND '".$data['tdate']."'");
	     	return $result;
	    }

	    
	}

?>