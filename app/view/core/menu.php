        <!-- Second navbar -->

        <div class="navbar navbar-inverse navbar-transparent" id="navbar-second">

            <ul class="nav navbar-nav visible-xs-block">

                <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-paragraph-justify3"></i></a></li>

            </ul>



            <div class="navbar-collapse collapse" id="navbar-second-toggle">

                <ul class="nav navbar-nav navbar-nav-material">

                    

                    <?php

                        if (session_status() == PHP_SESSION_NONE) {

                            session_start();

                        }

                        if ($_SESSION["login_level"] == 'admin') {

                    ?>

                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                Data Master <span class="caret"></span>

                            </a>                            

                            <ul  class="dropdown-menu width-250" >

                                <li ><a href="../supplier">Supplier</a></li>

                                <li><a href="../kategori-barang">Kategori Barang</a></li>

                                <li><a href="../barang">Barang</a></li>

                                <li><a href="../stok-barang">Stok Barang</a></li>

                                <li><a href="../pengguna">Pengguna</a></li>

                                <li><a href="../konsumen">Konsumen</a></li>

                            </ul>

                        </li>

                        <li><a href="../servis">Servis</a></li>

                        <li><a href="../pemesanan">Pemesanan</a></li>

                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                Laporan <span class="caret"></span>

                            </a>                            

                            <ul class="dropdown-menu width-250">

                                <li><a href="../laporan/penjualan.php">Laporan Penjualan Barang</a></li>

                                <li><a href="../laporan/servis.php">Laporan Servis</a></li>

                                <li><a href="../laporan/stok.php">Laporan Stok</a></li>

                            </ul>

                        </li>

                    <?php



                        }else if ($_SESSION["login_level"] == 'pemilik') {

                    ?>

                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                    Laporan <span class="caret"></span>

                                </a>                            

                                <ul class="dropdown-menu width-250">

                                    <li><a href="../laporan/penjualan.php">Laporan Penjualan Barang</a></li>

                                    <li><a href="../laporan/servis.php">Laporan Servis</a></li>

                                </ul>

                            </li>

                    <?php



                        }else {

                    ?>  

                            <li><a href="../utama/index.php">Komponen Hardware Komputer</a></li>

                    <?php

                        }

                    ?>

                </ul>

            </div>

        </div>

        <!-- /second navbar -->



