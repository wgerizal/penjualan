<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistem Penjualan</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/datepicker.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/ui/nicescroll.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/ui/drilldown.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/ui/fab.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/inputs/touchspin.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/styling/switch.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/notifications/noty.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/pickers/anytime.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/pickers/pickadate/picker.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/pickers/pickadate/picker.date.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/pickers/pickadate/picker.time.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/pickers/pickadate/legacy.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/tables/datatables/datatables.min.js"></script>

    <script type="text/javascript" src="../../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../../../assets/js/pages/datatables_basic.js"></script>
    <!-- /theme JS files -->

</head>

<body class="navbar-bottom">

    <!-- Page header -->
    <div class="page-header page-header-inverse">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse navbar-transparent">
            <div class="navbar-header">
                <ul class="nav navbar-nav">
                    <li><a href="#" style="font-size: 18px;color: #58b5ff;">Toko Febby Komputer</a></li>
                </ul>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-grid3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">

                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-bell2"></i>
                                <span class="visible-xs-inline-block position-right">Activity</span>
                                <span class="status-mark border-pink-300"></span>
                            </a>

                            <div class="dropdown-menu dropdown-content">
                                <div class="dropdown-content-heading">
                                    Notifikasi
                                    <ul class="icons-list">
                                        <li><a href="#"><i class="icon-menu7"></i></a></li>
                                    </ul>
                                </div>

                                <ul class="media-list dropdown-content-body width-350">
                                    <li class="media">
                                        <div class="media-left">
                                            <a href="#" class="btn bg-purple-300 btn-rounded btn-icon btn-xs"><i class="icon-truck"></i></a>
                                        </div>
                                        
                                        <div class="media-body">
                                            Shipping cost to the Netherlands has been reduced, database updated
                                            <div class="media-annotation">Feb 8, 11:30</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li> -->

                        <li class="dropdown dropdown-user">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <img src="../../../assets/images/ava.png" alt="">
                                <span><?php echo ucfirst($_SESSION["login_nama"]); ?></span>
                                <i class="caret"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="../../controller/LoginController.php?func=logout"><i class="icon-switch2"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /main navbar -->

        <?php require_once('menu.php');  ?>
        
    </div>
    <!-- /page header -->