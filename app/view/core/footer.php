    <!-- Footer -->
    <div class="navbar navbar-default navbar-fixed-bottom footer">
        <ul class="nav navbar-nav visible-xs-block">
            <li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="footer">
            <div class="navbar-text">
                &copy; Copyright © 2017. <a href="#" class="navbar-link">Toko Febby Komputer. All rights reserved. </a>
            </div>
        </div>
    </div>
    <!-- /footer -->

    <script type="text/javascript" src="../../../assets/js/bootstrap-datepicker.js"></script>
 