<?php
	if (session_status() == PHP_SESSION_NONE) {
	    session_start();
	}

	if (!empty($_SESSION['keranjang'])) {
		$total = count($_SESSION['keranjang']);
	}else{
		$total = 0;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sistem Penjualan</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="../../../assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/loaders/blockui.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/ui/nicescroll.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/ui/drilldown.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/ui/fab.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="../../../assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/forms/validation/validate.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="../../../assets/js/core/app.js"></script>
	<script type="text/javascript" src="../../../assets/js/pages/login.js"></script>
	<script type="text/javascript" src="../../../assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

</head>

<div class="header_top">
	<div class="row">
		<ul class="nav nav-pills">
			<ul class="nav navbar-nav navbar-left">
				<li class="dropdown">
					<a href="#" style="font-size: 14px;color:black;font-family: calibri light"class="dropdown-toggle" >
						0821-1752-2282 <i class="icon-phone2"></i>
					</a>
				</li>
			</ul>
	<ul class="nav navbar-nav navbar-left">
				<li class="dropdown">
					<a href="#" style="font-size: 14px;color:black;font-family: calibri light"class="dropdown-toggle" >
						febbykomputer@gmail.com <i class="icon-envelope"></i>
					</a>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" style="font-size: 20px;color:black;font-family: calibri light"class="dropdown-toggle" >
						<i class="icon-facebook2"></i>
					</a>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" style="font-size: 20px;color:black;font-family: calibri light"class="dropdown-toggle" >
						<i class="icon-twitter"></i>
					</a>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" style="font-size: 20px;color:black;font-family: calibri light"class="dropdown-toggle" >
						<i class="icon-instagram"></i>
					</a>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" style="font-size: 20px;color:black;font-family: calibri light"class="dropdown-toggle" >
						<i class="icon-google-plus"></i>
					</a>
				</li>
			</ul>
			<h1 style="font-family: calibri light" align="center">Selamat Datang Di Website <span style="color: orange;">Febby</span><span style="color: black;">Komputer</span></h1>
		</ul>

	</ul>
</div>

<body class="navbar-bottom login-container">


	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<ul class="nav navbar-nav">
				<li><a href="home.php" style="font-size: 18px;color: orange;font-family: calibri light"> Febby<span style="color:lightgray">Komputer</span></a></li>
			</ul>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-grid3"></i></a></li>
			</ul>
		</div>

			<div  class="navbar-collapse collapse" id="navbar-mobile">
				<ul  class="nav navbar-nav">
				<li ><a href="home.php" style="font-size: 18px;color: lightgray;font-family: calibri light">Home</a></li>
				<li><a href="index.php" style="font-size: 18px;color: lightgray;font-family: calibri light">Komponen Hardware Komputer</a></li>
				<li><a href="servis.php" style="font-size: 18px;color: lightgray;font-family: calibri light">Servis Komputer</a></li>
				<li ><a href="kontak.php" style="font-size: 18px;color: lightgray;font-family: calibri light">Kontak Kami</a>
												
				<li>
				<?php
				if (session_status() == PHP_SESSION_NONE) {
				    session_start();
				}
                if (isset($_SESSION['login_id']) && !empty($_SESSION['login_id'])) {
                  echo '<a href="../../controller/LoginController.php?func=logout" style="font-size: 18px;color: lightgray;font-family: calibri light">Logout</a>'; 
                }else{
                  echo '<a href="login.php" style="font-size: 18px;color: lightgray;font-family: calibri light">Login/Daftar</a>';
                }
                ?>
				</li>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="keranjang.php" style="font-size: 18px;color:lightgray;font-family: calibri light"class="dropdown-toggle" >
						Keranjang <i class="icon-cart5"></i>
						<span class="visible-xs-inline-block position-right">Messages</span>
						<?php
							if ($total != 0) {
						?>
							<span class="badge bg-warning-400"><?php echo $total; ?></span>
						<?php
							}
						?>
						
					</a>
				</li>
			</ul>
		</div>

	</div>
	<!-- /main navbar -->



