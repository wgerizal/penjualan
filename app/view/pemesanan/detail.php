<?php
    require_once('../../controller/PemesananController.php');
    $data = $PemesananController->detail();
    require_once "../core/header.php"; 
?>
       
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Form validation -->
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h5 class="panel-title">Detail Data <?php echo $data['title']; ?></h5>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                      </ul>
                    </div>
                  </div>

                  <div class="panel-body">
                    <form class="form-horizontal form-validate-jquery" action="<?php echo $PemesananController->path_controller; ?>" method="POST">
                      <fieldset class="content-group">
                        <legend class="text-bold"></legend>

                        <input type="hidden" name="post_type" value="add">
                        <input type="hidden" name="id" value="0">
                        <input type="hidden" name="func" value="save">
                        <!-- Basic text input -->
                        <div class="form-group">
                          <label class="control-label col-lg-3">Nama Konsumen</label>
                          <div class="col-lg-9">
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo $data['pemesanan'][0]['nama_konsumen']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Kode Pemesanan</label>
                          <div class="col-lg-9">
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo $data['pemesanan'][0]['kode_pemesanan']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Total Bayar</label>
                          <div class="col-lg-9">
                            <input type="text" class="form-control" readonly="readonly" value="Rp. <?php echo $data['pemesanan'][0]['total_bayar']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Jenis Pengiriman</label>
                          <div class="col-lg-9">
                            <input type="text" class="form-control" readonly="readonly" value="<?php if($data['pemesanan'][0]['jenis_pengiriman']=='COD'){echo "Ambil Di Toko";}else{echo $data['pemesanan'][0]['jenis_pengiriman'];} ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Status Pengiriman</label>
                          <div class="col-lg-9">
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo $data['pemesanan'][0]['status_pengiriman']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Status Pembayaran</label>
                          <div class="col-lg-9">
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo $data['pemesanan'][0]['status_pembayaran']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Bukti Pembayaran</label>
                          <div class="col-lg-9">

                            <?php
                              if ($data['pemesanan'][0]['bukti_pembayaran'] != NULL || $data['pemesanan'][0]['bukti_pembayaran'] != "") {
                            ?>
                                <a href="../../../uploads/bukti/<?php echo $data['pemesanan'][0]['bukti_pembayaran']; ?>" target="_blank"> <img src="../../../uploads/bukti/<?php echo $data['pemesanan'][0]['bukti_pembayaran']; ?>" style="width: 200px;height: auto;"></a>
                            <?php    
                              }else{
                            ?>
                                <input type="text" class="form-control" readonly="readonly" value="-">
                            <?php                                    
                              }
                            ?>
                            
                          </div>
                        </div>
                        <hr>
                        <h5 class="panel-title">Detail Pemesanan Produk</h5>
                        <br>
                        <div class="row">

                            <?php
                            foreach ($data['detailPemesanan'] as $v) {
                            ?>
                                <div class="col-xs-8">
                                  <div class="form-group">
                                    <label class="control-label col-lg-3">Nama Produk</label>
                                    <div class="col-lg-9">
                                      <input type="text" class="form-control" readonly="readonly" value="<?php echo $v['nama_barang']; ?>">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-xs-4">
                                  <div class="form-group">
                                    <label class="control-label col-lg-4">Quantity Produk</label>
                                    <div class="col-lg-8">
                                      <input type="text" class="form-control" readonly="readonly" value="<?php echo $v['jumlah']; ?>">
                                    </div>
                                  </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        <hr>
                        
                        <!-- /basic text input -->

                      </fieldset>

                      <div class="text-center">
                        <a href="index.php" class="btn btn-default">Kembali</a>
                        <?php

                          if (strtolower($data['pemesanan'][0]['status_pengiriman']) == 'menunggu' && strtolower($data['pemesanan'][0]['status_pembayaran']) != 'belum lunas' && strtolower($data['pemesanan'][0]['jenis_pengiriman']) == "pengiriman online") {
                        ?>
                          <form action="<?php echo $PemesananController->path_controller; ?>" method="POST" style="display: inline;">
                            <input type="hidden" name="id_pemesanan" value="<?php echo $data['pemesanan'][0]['id_pemesanan']; ?>">
                            <input type="hidden" name="func" value="konfirmasi_pengiriman">
                            <button type="submit" class="btn btn-info">Konfirmasi Pengiriman</button>  
                          </form>
                          
                        <?php
                          }
                        ?>
                        <?php
                          if (strtolower($data['pemesanan'][0]['status_pembayaran']) == 'belum lunas' && $data['pemesanan'][0]['bukti_pembayaran'] != "" && strtolower($data['pemesanan'][0]['jenis_pengiriman']) == "pengiriman online" ) {
                        ?>
                            <form action="<?php echo $PemesananController->path_controller; ?>" method="POST" style="display: inline;">
                              <input type="hidden" name="id_pemesanan" value="<?php echo $data['pemesanan'][0]['id_pemesanan']; ?>">
                              <input type="hidden" name="func" value="konfirmasi_pembayaran">
                              <button type="submit" class="btn btn-primary">Konfirmasi Pembayaran</button>  
                            </form>
                        <?php
                          }
                        ?>
                        <?php
                          if (strtolower($data['pemesanan'][0]['status_pembayaran']) == 'belum lunas' && $data['pemesanan'][0]['jenis_pengiriman'] == "COD" ) {
                        ?>
                            <form action="<?php echo $PemesananController->path_controller; ?>" method="POST" style="display: inline;">
                              <input type="hidden" name="id_pemesanan" value="<?php echo $data['pemesanan'][0]['id_pemesanan']; ?>">
                              <input type="hidden" name="func" value="konfirmasi_pembayaran">
                              <button type="submit" class="btn btn-primary">Konfirmasi Pembayaran</button>  
                            </form>
                        <?php
                          }
                        ?>
                        <form action="<?php echo $PemesananController->path_controller; ?>" method="POST" style="display: inline;">
                              <input type="hidden" name="id_pemesanan" value="<?php echo $data['pemesanan'][0]['id_pemesanan']; ?>">
                              <input type="hidden" name="func" value="pembayaran_gagal">
                              <button type="submit" class="btn btn-danger">Pembayaran Gagal</button>  
                            </form>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- /form validation -->
                

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


<?php require_once('../core/footer.php');  ?>
    
    <script type="text/javascript">
      $(document).ready(function(){

        $(".file-styled-primary").uniform({
          fileButtonClass: 'action btn bg-blue'
        });

        var validator = $(".form-validate-jquery").validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-error-label',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {

                // Styled checkboxes, radios, bootstrap switch
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                    if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo( element.parent().parent().parent().parent() );
                    }
                     else {
                        error.appendTo( element.parent().parent().parent().parent().parent() );
                    }
                }

                // Unstyled checkboxes, radios
                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo( element.parent().parent().parent() );
                }

                // Input with icons and Select2
                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo( element.parent() );
                }

                // Inline checkboxes, radios
                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent() );
                }

                // Input group, styled file input
                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo( element.parent().parent() );
                }

                else {
                    error.insertAfter(element);
                }
            },
            validClass: "validation-valid-label"
        });

      });
    </script>
</body>
</html>
