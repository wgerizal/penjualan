<?php
    require_once('../../controller/HalamanUtamaController.php');
    $data = $HalamanUtamaController->index();
    require_once "../core/header.php"; 
?>
       
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Quick stats boxes -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel bg-blue-400">
                            <div class="panel-body">
                                <h2 style="text-align: center">SELAMAT DATANG ANDA LOGIN SAAT INI PADA </h2>
                                <h2 style="text-align: center"><?php echo date('d F Y'); ?></h2>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- /quick stats boxes -->
                

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


<?php require_once('../core/footer.php');  ?>

</body>
</html>
