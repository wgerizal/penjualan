<?php
    require_once('../../controller/KaryawanController.php');
    $data = $KaryawanController->detail();
    require_once "../core/header.php"; 
?>
       
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <div class="row">
                    <div class="col-sm-3">

                      <!-- User thumbnail -->
                      <div class="thumbnail">
                        <div class="thumb thumb-rounded thumb-slide">
                          <img src="../../../uploads/image/<?php echo $data[$KaryawanController->table_data][0]['foto']; ?>" alt="">
                          
                        </div>
                      
                          <div class="caption text-center">
                            <h6 class="text-semibold no-margin"><?php echo ucfirst($data[$KaryawanController->table_data][0]['nama_karyawan']); ?> <small class="display-block"><?php echo ucfirst($data[$KaryawanController->table_data][0]['nama_jabatan']); ?></small></h6>
                          </div>
                        </div>
                        <!-- /user thumbnail -->

                    </div>
                    <div class="col-sm-9">

                        <!-- Navigation -->
                        <div class="panel panel-flat">
                          <div class="panel-heading">
                            <h6 class="panel-title"><b>Data Karyawan</b></h6>
                          </div>

                          <div class="list-group no-border no-padding-top">
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    NPK
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['nama_karyawan']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Tanggal Masuk
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['tanggal_masuk']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Status Karyawan
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['status_karyawan']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <?php
                              if ($data[$KaryawanController->table_data][0]['status_karyawan'] == 'kontrak') {
                            ?>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Tanggal Awal Kontrak
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['tanggal_awal_kontrak']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Tanggal Akhir Kontrak
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['tanggal_akhir_kontrak']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <?php
                              }
                            ?>
                            <div class="list-group-divider"></div>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Jenis Kelamin
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['jenis_kelamin']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Tempat Lahir
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['tempat_lahir']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Tanggal Lahir
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['tanggal_lahir']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Tingkat Pendidikan
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['tingkat_pendidikan']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Jurusan Pendidikan
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['jurusan_pendidikan']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Alamat
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['alamat']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    No Telepon
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['no_telp']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Email
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo $data[$KaryawanController->table_data][0]['email']; ?>
                                  </div>
                                </div>
                              </div>
                            </a>    
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Agama
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['agama']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>    
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    No Telepon
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['no_telp']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>    
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Prestasi
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['prestasi']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>        
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    File CV
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <span id="download" style="color: blue;text-decoration: underline;">Download</span>
                                  </div>
                                </div>
                              </div>
                            </a>        
                            <div class="list-group-divider"></div>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Username
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['username']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                            <a href="#" class="list-group-item">
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="col-sm-3">
                                    Level Pengguna
                                  </div>
                                  <div class="col-sm-1 text-right">
                                    :
                                  </div>
                                  <div class="col-sm-8">
                                    <?php echo ucfirst($data[$KaryawanController->table_data][0]['level_pengguna']); ?>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </div>
                        </div>
                        <!-- /navigation -->
                      
                    </div>
                </div>
                
                <div class="text-right"><a href="index.php" class="btn btn-primary" >Kembali</a></div>
                

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


<?php require_once('../core/footer.php');  ?>
    
    <script type="text/javascript">
      $(document).ready(function(){


          <?php 
            $url = $_SERVER['REQUEST_URI']; //returns the current URL
            $parts = explode('/',$url);
            $dir = $_SERVER['SERVER_NAME'];
            for ($i = 0; $i < count($parts) - 1; $i++) {
              if ($parts[$i] == "app") {
                break;
              }else{
                $dir .= $parts[$i] . "/";   
              }
             
            }
            
          ?>

          $( "#download" ).click(function() {
            <?php echo 'window.open("'.'http://'.$dir.'uploads/document/'.$data[$KaryawanController->table_data][0]['file_cv'].'","_blank");'; ?>
          });

      });
    </script>
</body>
</html>
