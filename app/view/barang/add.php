<?php
    require_once('../../controller/BarangController.php');
    $data = $BarangController->add();
    require_once "../core/header.php"; 
?>
       
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Form validation -->
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h5 class="panel-title">Add Data <?php echo $data['title']; ?></h5>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                      </ul>
                    </div>
                  </div>

                  <div class="panel-body">
                    <form class="form-horizontal form-validate-jquery" action="<?php echo $BarangController->path_controller; ?>" method="POST" enctype="multipart/form-data">
                      <fieldset class="content-group">
                        <legend class="text-bold"></legend>

                        <input type="hidden" name="post_type" value="add">
                        <input type="hidden" name="id" value="0">
                        <input type="hidden" name="func" value="save">

                        <!-- Basic text input -->
                        <div class="form-group">
                          <label class="control-label col-lg-3">Nama Supplier <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <select name="id_supplier" id="id_supplier" class="form-control" required="required">
                                <?php
                                  foreach ($data['supplier'] as $v) {
                                    echo '<option value="'.$v['id_supplier'].'">'.$v['nama_supplier'].'</option>';
                                  }
                                ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Kategori Barang <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <select name="id_kategori_barang" id="id_kategori_barang" class="form-control" required="required">
                                <?php
                                  foreach ($data['kategoriBarang'] as $v) {
                                    echo '<option value="'.$v['id_kategori_barang'].'">'.$v['nama_kategori'].'</option>';
                                  }
                                ?>
                            </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-lg-3">Nama Barang <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <input type="text" name="nama_barang" id="nama_barang" class="form-control" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-lg-3">Harga Barang <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <input type="number" name="harga_barang" id="harga_barang" class="form-control" required="required" min="0">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-lg-3">Keterangan Barang <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <input type="text" name="keterangan_barang" id="keterangan_barang" class="form-control" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-lg-3">Gambar Barang <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <input type="file" class="form-control file-styled-primary" name="ffoto" id="ffoto" required="required">
                          </div>
                        </div>
                        <!-- /basic text input -->

                      </fieldset>

                      <div class="text-right">
                        <a href="index.php" class="btn btn-default" >Cancel</a>
                        <button type="submit" class="btn btn-primary" id="btnAdd">Submit <i class="icon-arrow-right14 position-right"></i></button>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- /form validation -->
                

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


<?php require_once('../core/footer.php');  ?>
    
    <script type="text/javascript">
      $(document).ready(function(){

        $(".file-styled-primary").uniform({
          fileButtonClass: 'action btn bg-blue'
        });

        var validator = $(".form-validate-jquery").validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-error-label',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {

                // Styled checkboxes, radios, bootstrap switch
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                    if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo( element.parent().parent().parent().parent() );
                    }
                     else {
                        error.appendTo( element.parent().parent().parent().parent().parent() );
                    }
                }

                // Unstyled checkboxes, radios
                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo( element.parent().parent().parent() );
                }

                // Input with icons and Select2
                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo( element.parent() );
                }

                // Inline checkboxes, radios
                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent() );
                }

                // Input group, styled file input
                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo( element.parent().parent() );
                }

                else {
                    error.insertAfter(element);
                }
            },
            validClass: "validation-valid-label"
        });

        $( "#btnAdd" ).click(function() {
            if ($('.form-validate-jquery').valid()) {
                var ext = $('#ffoto').val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
                    alert('Extension input file foto tidak diperbolehkan! (contoh: .png,.jpg,.jpeg)');
                    return false;
                }
                
            }
        });

      });
    </script>
</body>
</html>
