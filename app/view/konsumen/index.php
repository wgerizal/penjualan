<?php
    require_once('../../controller/KonsumenController.php');
    $data = $KonsumenController->index();
    require_once "../core/header.php"; 
?>
       
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic datatable -->
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h5 class="panel-title"> Data <?php echo $data['title']; ?></h5>
                    <div class="heading-elements">
                      <ul class="icons-list">
                      </ul>
                    </div>
                  </div>

                  <div class="panel-body">
                  </div>

                  <table class="table datatable-basic">
                    <thead>
                      <tr>
                        <th>No</th>
                        <?php
                          foreach ($KonsumenController->table_title as $t) {
                            echo '<th>'.$t.'</th>';
                          }
                        ?>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          $no = 1;
                          foreach ($data[$KonsumenController->table_data] as $d) {
                      ?>
                              <tr>
                                  <td><?php echo $no; ?></td>
                                  <?php
                                    foreach ($KonsumenController->table_field as $f) {
                                      echo '<td>'.$d[$f].'</td>';
                                      
                                    }
                                  ?>
                                
                              </tr>
                      <?php
                              $no++;
                          }
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /basic datatable -->
                

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


<?php require_once('../core/footer.php');  ?>
    
    <script type="text/javascript">
      $(document).ready(function(){

        $('.datatable-basic').DataTable();

        $.fn.delete = function(id) {    

            if (confirm("Apakah anda yakin mengahapus data ini?")) {
                var form_data = {            
                    id:id
                };
                $.ajax({
                    type: "POST",
                    url: "<?php echo $KonsumenController->path_controller; ?>?func=delete",
                    data: form_data,
                    success: function(response)
                    {
                        setTimeout(' window.location.href = "index.php"; ',0);          
                    } 
                });
            }
            
            return false;
        }

        <?php 
            if (isset($_SESSION["notification_message"]) && !empty($_SESSION["notification_message"])) {
        ?>
                
                var text = "<?php echo $_SESSION["notification_message"]; ?>";
                var layout = "topRight";
                <?php 
                    if ($_SESSION["notification_result"] === 'success') {
                ?>
                        var type = "success";
                <?php 
                    }else{
                ?>
                        var type = "error";
                <?php 
                    }

                    unset($_SESSION["notification_result"]);
                    unset($_SESSION["notification_message"]);
                ?>

                noty({
                    width: 200,
                    text: text,
                    type: type,
                    dismissQueue: true,
                    timeout: 3000,
                    layout: layout
                });
                return false;
                
        <?php
            }
        ?>

      });
    </script>
</body>
</html>
