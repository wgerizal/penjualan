<style type="text/css">
    h1,h3,h5{
        text-align: center;

    }
 
      .short{
        width: 50px;
      }
 
      table{
        border-collapse: collapse;
        font-family: arial;
        color:#5E5B5C;
      }
 
      thead th{
        text-align: left;
        padding: 10px;
      }
 
      tbody td{
        border-top: 1px solid #e3e3e3;
        padding: 10px;
        
      }
 
      tbody tr:nth-child(even){
        background: #F6F5FA;

      }
 
      tbody tr:hover{
        background: #EAE9F5

      }
</style>
<page>
<h1>Toko Feby Komputer</h1>
<h5><b>Alamat: Jl. Ibrahim Adjie No 47, Kiaracondong Bandung Blok CC-115 Bandung Trade Mall. </b></h5>
<h5>Email: febbykomputer@gmail.com. No Telepon: 0821-1752-2282 </h5>

<h5>_________________________________________________________________________________________________________________________________________________________</h5>
<h3>Laporan Penjualan Komponen Hardware Komputer</h3>
    <h3><?php echo $data['fdate']." sampai ".$data['tdate']; ?></h3>
    <div id="outtable">
    </div>
    <table border="1" align="center">

        <thead>
            <tr>
                <th class="normal">Nama Konsumen</th>
                <th class="normal">Kode Pemesanan</th>
                <th class="normal">Jenis Pengiriman</th>
                <th class="normal">Status Pengiriman</th>
                <th class="normal">Status Pembayaran</th>
                <th class="normal">Tanggal</th>
                <th class="normal">Total Bayar</th>
            </tr>
        </thead>
        <tbody>
            
            <?php
                $total=0;
                foreach ($data['pemesanan'] as $v) {
            ?>
                    <tr>
                        <td><?php echo $v['nama_konsumen']; ?></td>
                        <td><?php echo $v['kode_pemesanan']; ?></td>
                        <td><?php echo $v['jenis_pengiriman']; ?></td>
                        <td><?php echo $v['status_pengiriman']; ?></td>
                        <td><?php echo $v['status_pembayaran']; ?></td>
                        <td><?php echo $v['tanggal']; ?></td>
                        <td>Rp. <?php echo $v['total_bayar']; ?></td>
                                            </tr>
            <?php
                  $total=$total+$v['total_bayar'];
                }
            ?>
            <tr>
              <td colspan="5"></td>
              <td>Total</td>
              <td>Rp. <?php echo $total; ?>
              </td>
            </tr>
            
</tbody>
</table>
</page>


   
 <br>
    <table align="right">

  <tr>      
      <td><b>Bandung, ...........................</b></td>
    </tr>

    <br>
    <tr>    
      <td><b>Mengetahui, Pemilik Toko.</b></td>
    </tr>
    </table> 

   <br><br><br><br>
    <table align="right">
   <tr>    
  <td>(  Rizki Nanda Saputra  )</td>
  </tr>
  </table>
