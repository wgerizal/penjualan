<?php
    require_once('../../controller/FKeranjangController.php');
    $data = $FKeranjangController->index();
    require_once "../core/header_utama.php"; 
?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<div class="row">
					<div class="col-lg-6 col-lg-offset-3">
						<center>
						<h3 style="margin-top: 0px;">Daftar Pembelian</h3>
						</center>
					</div>
					<br>
				</div>
				<div style="position: fixed;right: 55px;top: 60px;border: 3px solid #979797;padding: 15px;width: 300px;margin: 10px auto;background: white;font-size: 11px;">
					<h4 style="margin-top: 0px;color: #FC411D;margin-bottom: 10px;padding-bottom: 10px;font-size: 14px;border-bottom: 1px solid #eee;">REKAP BELANJA</h4>
					<span style="">Total Belanja</span>
					<span style="float: right;"><b>Rp. <span id="total">0</span></b></span>
				</div>
				<div class="row" style="margin-top: 30px;background-color: #FFFFFF;padding: 20px;">
					<div class="col-md-12" style="color: red;">
						<center>
							<i class="icon-close2"></i><a href="../../controller/FKeranjangController.php?func=kosongin" style="color: red;"> Kosongkan Kerangjang</a>
						</center>
						
					</div>
				</div>
				<form method="POST" action="../../controller/FKeranjangController.php?func=lanjut">
					
				
					<?php 
						$i = 1;
						foreach ($data['keranjang'] as $v) {
					?>
						<input type="hidden" name="barang_id[]" value="<?php echo $v['id_barang']; ?>">
						<div class="row" style="background-color: #FFFFFF;padding: 20px;">
							<div class="col-md-3 col-md-offset-3">
								<center>
									<a href="#" style="color: #000000;font-size: 16px;display: block;"><?php echo $v['nama_barang']; ?></a><br>
									<img src="../../../uploads/image/<?php echo $v['gambar']; ?>" style="width: 100px;height: auto;">
									<div style="margin-top: 20px;">Rp. <span id="harga<?php echo $i; ?>"><?php echo $v['harga_barang']; ?></span></div>
									<div>Stok - <?php echo $v['jumlah']; ?></div>
								</center>
							</div>
							<div class="col-md-6">
								Jumlah &nbsp
								<input type="number" class="kuantity" id="qty<?php echo $i; ?>" name="qty[]" style="width:50px;" min="0" value="1" max="<?php echo $v['jumlah']; ?>">
								<a href="../../controller/FKeranjangController.php?func=delete&id_barang=<?php echo $v['id_barang']; ?>" style="color: red;"> Delete</a>
							</div>
						</div>

					<?php
						$i++;		
						}
					?>


					<div class="row" style="margin-top: 30px;">
						<div class="col-lg-6 col-lg-offset-3">
							<center>
								<h3 style="margin-top: 0px;font-weight: 500;color: #FC411D;">Pilih Pengiriman</h3>
								<p>Silahkan pilih salah satu jenis pengiriman yang Anda inginkan</p>

							</center>

							<div style="background-color: #F2300C;padding: 10px;width: 310px;margin: 0 auto;">
									<div class="radio" style="color: white;display: inline;">
									  <label><input type="radio" name="jenis_pengiriman" checked="checked" value="online">Pengiriman Online</label>
									</div>
									<div class="radio" style="color: white;display: inline;margin-left: 20px;">
									  <label><input type="radio" name="jenis_pengiriman" value="cod">Ambil Di Toko</label>
									</div>
									
							</div>
						</div>
						<br>
					</div>


					<div class="row" style="background-color: #FFFFFF;padding: 10px;margin-top: 20px;">
						<div class="col-md-12" style="">
							<center>
								<a href="index.php" class="btn btn-info" style="font-size: 16px;">Lanjut Belanja</a>
							<button type="submit" class="btn btn-primary" style="font-size: 16px;">Lanjut ke Pembayaran</button>
							</center>
						</div>
					</div>
				</form>


			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<?php require_once('../core/footer_utama.php');  ?>

	<script type="text/javascript">
		$(function() {
			$('.select').select2({});

			$( ".kuantity" ).change(function() {

				jum = <?php echo count($data['keranjang']); ?> ;
			  	total = 0;

			  	for (var i = 1; i <= jum; i++) {

			  		varHarga  = "#harga"+i;
			  		harga = $( varHarga ).html();
			  		varQty  = "qty"+i;
			  		qty = $( "#"+varQty ).val();
			  		total = total + (harga*qty);

			  		$( "#total").html(total);

			  	}

			});

			$( ".kuantity" ).change();

		});
	</script>

</body>
</html>
