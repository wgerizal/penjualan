<?php

    require_once "../core/header_utama.php"; 

?>



	<!-- Page container -->

	<div class="page-container">



		<!-- Page content -->

		<div class="page-content">



			<!-- Main content -->

			<div class="content-wrapper">



				<!-- Simple login form -->

				<form action="#" method="POST" class="form-validate login-form">

					<div class="panel panel-body">

						<div class="text-center">

							<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>

							<h5 class="content-group">Login <small class="display-block">Masukan akun form dibawah ini</small></h5>

						</div>



						<div class="form-group has-feedback has-feedback-left">

							<input type="text" class="form-control" placeholder="Email" id="email" name="email">

							<div class="form-control-feedback">

								<i class="icon-user text-muted"></i>

							</div>

						</div>



						<div class="form-group has-feedback has-feedback-left">

							<input type="password" class="form-control" placeholder="Password" id="password" name="password">

							<div class="form-control-feedback">

								<i class="icon-lock2 text-muted"></i>

							</div>

						</div>



						<div class="form-group">

							<button type="submit" class="btn bg-orange-400 btn-block" id="btnLogin">Masuk <i class="icon-circle-right2 position-right"></i></button>

						</div>



						<div class="content-divider text-muted form-group"><span>Tidak punya akun?</span></div>

						<a href="daftar.php" class="btn btn-default btn-block content-group">Daftar</a>



						

					</div>

				</form>

				<!-- /simple login form -->



			</div>

			<!-- /main content -->



		</div>

		<!-- /page content -->



	</div>

	<!-- /page container -->





	<?php require_once('../core/footer_utama.php');  ?>



	<script type="text/javascript">

		$(document).ready(function(){

			// Setup validation

		    $(".form-validate").validate({

		        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields

		        errorClass: 'validation-error-label',

		        successClass: 'validation-valid-label',

		        highlight: function(element, errorClass) {

		            $(element).removeClass(errorClass);

		        },

		        unhighlight: function(element, errorClass) {

		            $(element).removeClass(errorClass);

		        },



		        // Different components require proper error label placement

		        errorPlacement: function(error, element) {



		            // Styled checkboxes, radios, bootstrap switch

		            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {

		                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {

		                    error.appendTo( element.parent().parent().parent().parent() );

		                }

		                 else {

		                    error.appendTo( element.parent().parent().parent().parent().parent() );

		                }

		            }



		            // Unstyled checkboxes, radios

		            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {

		                error.appendTo( element.parent().parent().parent() );

		            }



		            // Input with icons and Select2

		            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {

		                error.appendTo( element.parent() );

		            }



		            // Inline checkboxes, radios

		            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {

		                error.appendTo( element.parent().parent() );

		            }



		            // Input group, styled file input

		            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {

		                error.appendTo( element.parent().parent() );

		            }



		            else {

		                error.insertAfter(element);

		            }

		        },

		        validClass: "validation-valid-label",

		        rules: {

		            email: {

		                required:true

		            },

		            password: {

		                required:true

		            }

		        },

		        messages: {

		            email: {

		            	required: "Email harus diisi"

		            },

		            password: {

		            	required: "Password harus diisi",

		            }

		        }

		    });



		    $( "#btnLogin" ).click(function() {

                if ($('.form-validate').valid()) {

                    var action = "../../controller/LoginController.php?func=login";

                    var form_data = {

                      email: $("#email").val(),

                      password: $("#password").val(), 

                    };



                    console.log(form_data);

                    $.ajax({

                       type: "POST",

                       url: action,

                       data: form_data,

                       success: function(response)

                       {

                            console.log(response);



                            if (response === '1') {

                                if (alert('Login Berhasil')) {

                                    setTimeout(' window.location.href = "../dashboard"; ',0);  

                                }else{

                                    setTimeout(' window.location.href = "../dashboard"; ',0);  

                                }

                            }else if(response === '2'){

                            	setTimeout(' window.location.href = "index.php"; ',0);  

                        	}else if(response === '3'){

                            	setTimeout(' window.location.href = "pembayaran.php"; ',0);  

                        	}else{

                                alert('Kombinasi email dan password salah')

                            }

                          

                       } 

                    });

                    return false;

                }

                return false;

            });

		});

	</script>



</body>

</html>

