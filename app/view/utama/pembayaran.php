<?php
	if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    if (!empty($_SESSION['pemesanan'])) {
    	$jenis_pengiriman = $_SESSION['pemesanan']['jenis_pengiriman'];
    	if ($jenis_pengiriman == 'online') {
    		$jenis_pengiriman = "Pengiriman Online";
    	}
    	
    }else{
    	$jenis_pengiriman = "";
    }

    require_once "../core/header_utama.php"; 
?>

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<div class="row" style="margin-top: 20px;">
					<div class="col-md-6 col-md-offset-3">
						<form action="../../controller/FDaftarController.php?func=bayar" method="POST">
							<div class="panel panel-flat">

								<div class="panel-body">

									<?php
							            if (isset($_SESSION["notification_bayar"]) && !empty($_SESSION["notification_bayar"])) {
							        ?>
							                
							                <?php 
							                    if ($_SESSION["notification_bayar"] === 'success') {
							                ?>
							                        <div class="text-center">
														<div class="alert alert-success no-border">
															<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
															<span class="text-semibold">Pemesanan!</span> Anda berhasil. Silahkan Cek Email Anda untuk konfirmasi pembayaran.
													    </div>
													</div>
							                <?php 
							                    }else{
							                ?>
							                        <div class="alert alert-danger no-border">
														<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
														<span class="text-semibold">Maaf!</span> Pemesanan anda gagal.
												    </div>
							                <?php 
							                    }

							                    unset($_SESSION["notification_bayar"]);
							                ?>
							                
							        <?php
							            }else {
							        ?>

							        	<div class="form-group">
											<label>Jenis Pembayaran:</label>
											<input type="text" class="form-control" name="jenis_pembayaran" readonly="readonly" value="<?php echo $jenis_pengiriman; ?>">
										</div>

										<?php 
											if ($jenis_pengiriman == "Pengiriman Online") {
										?>
												<div id="ketkirim">
													<div class="form-group">
														<label>Nama Bank Asal:</label>
														<input type="text" name="bank_asal" class="form-control" required="required">
													</div>

													<div class="form-group">
														<label>Atas Nama:</label>
														<input type="text" name="atas_nama" class="form-control" required="required">
													</div>

													<div class="form-group">
														<label>No Rekening:</label>
														<input type="number" name="norek" class="form-control" required="required">
													</div>
												</div>
										<?php
											}
										?>

										<div class="text-right">
											<button type="submit" class="btn bg-orange-400">Bayar <i class="icon-arrow-right14 position-right"></i></button>
										</div>

							        <?php    	
							            }
							        ?>
									
									
									

									
								</div>
							</div>
						</form>
					</div>
				</div>
				

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<?php require_once('../core/footer_utama.php');  ?>

	<script type="text/javascript">
		$(function() {

			$('#jenis_pembayaran').change( function(){
				if ($('#jenis_pembayaran').val() == "Cash On Delivery") {
					$('#ketkirim').css("display","none");
				}else{
					$('#ketkirim').css("display","inherit");
				}
			});
		});
	</script>

</body>
</html>
