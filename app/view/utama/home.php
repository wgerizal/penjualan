<?php
    require_once "../core/header_utama.php"; 
?>

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

						<br>
						<br>
						<br>
					
				</br>
					<div class="col-sm-20 col-sm-offset-1">
						<section id="slider"><!--slider-->
							<div class="container">
								<div class="row">
									<div class="col-sm-12">
										<div id="slider-carousel" class="carousel slide" data-ride="carousel">
											<ol class="carousel-indicators">
												<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
												<li data-target="#slider-carousel" data-slide-to="1"></li>
												<li data-target="#slider-carousel" data-slide-to="2"></li>
												<li data-target="#slider-carousel" data-slide-to="3"></li>
											</ol>

																					
											<div class="carousel-inner">
												<div class="item active">
													<div class="col-sm-6">
														<h1 style="text-align: center;font-size: 30px;font-family: calibri light;"><span style="color: orange;">Febby</span><span style="color: black;">Komputer</span></h1> 
														<h2 style="font-size: 26px;color: black;font-family: calibri light"><b>Toko Online Penjualan Komponen Hardware Komputer</b></h2>
														<p style="font-size:18px; color: black;font-family: calibri light">Dapatkan Komponen Hardware Komputer dengan harga murah hanya disini</p>
													</div>
													<div class="col-sm-6">
														<img src="../../../uploads/gambar/komponen1.png" class="komponen img-responsive" alt="" />
													</div>
												</div>
												<div class="item">
													<div class="col-sm-6">
														<h1 style="text-align: center;font-size: 30px;font-family: calibri light;"><span style="color: orange;">Febby</span><span style="color: black;">Komputer</span></h1>
														<h2 style="font-size: 26px;color: black;font-family: calibri light"><b>Toko Online Penjualan Komponen Hardware Komputer</b></h2>
														<p style="font-size:18px; color: black;font-family: calibri light">Menjual Komponen Hardware Dan Servis Komputer</p>
													</div>
													<div class="contentl-sm-6">
														<img src="../../../uploads/gambar/komponen2.png" class="komponen img-responsive" alt="" />
													</div>
												</div>
												
												<div class="item">
													<div class="col-sm-6">
														<h1 style="text-align: center;font-size: 30px;font-family: calibri light;"><span style="color: orange;">Febby</span><span style="color: black;">Komputer</span></h1>
														<h2 h2 style="font-size: 26px;color: black;font-family: calibri light"><b>Toko Online Penjualan Komponen Hardware Komputer</b></h2>
														<p style="font-size:18px; color: black;font-family: calibri light">Dapatkan Kemudahan Berbelanja Di Website Toko Febby Komputer</p>
													</div>
													<div class="col-sm-6">
														<img src="../../../uploads/gambar/komponen3.png" class="komponen img-responsive" alt="" />
													</div>
												</div>
												
												<div class="item">
													<div class="col-sm-6">
														<h1 style="text-align: center;font-size: 30px;font-family: calibri light;"><span style="color: orange;">Febby</span><span style="color: black;">Komputer</span></h1>
														<h2 style="font-size: 26px;color: black;font-family: calibri light"><b>Toko Online Penjualan Komponen Hardware Komputer</b></h2>
														<p style="font-size:18px; color: black;font-family: calibri light">Cari Barang Yang Anda Sukai Dan Lakukan Pemesanan</p>
													</div>
													<div class="col-sm-6">
														<img src="../../../uploads/gambar/komponen4.png" class="komponen img-responsive" alt="" />
													</div>
												</div>
											</div>
											
											<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
												<i class="fa fa-angle-left"></i>
											</a>
											<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
												<i class="fa fa-angle-right"></i>
											</a>
										</div>
										
									</div>
								</div>
							</div>
							<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2 class style="text-align:left;font-size: 24px;color: orange;font-family: calibri light"=>Kontak Kami</h2>
							<ul class="nav nav-pills nav-stacked">
								<h5><a href="#" style="text-align:left;color: black;font-family: calibri light">BBM</a></h5>
								<h5><a href="#" style="text-align:left;color: black;font-family: calibri light">LINE</a></h5>
								<h5><a href="#" style="text-align:left;color: black;font-family: calibri light">FACEBOOK</a></h5>
								<h5><a href="#" style="text-align:left;color: black;font-family: calibri light">INSTRAGRAM</a></h5>
							</ul>
						</div>
					</div>
		
						<div class="row">
								<h2 class style="text-align:left;font-size: 24px;color: orange;font-family: calibri light">Tentang Kami</h2> 
								<ul  style="text-align:left;color: orange;font-family: calibri light" class=nav nav-pills nav-stacked>
									<h5><a href="sejarah.php" class style="color: black">Sejarah <span style="color: orange;">Febby</span><span style="color: black;">Komputer</span></h5>
									<h5><a href="kontak.php" class style="color: black	" ="navbar-link">Lokasi <span style="color: orange;">Febby</span><span style="color: black;">Komputer</span></h5>
								</ul>
								
							</div>
							<h2 class style="margin-top: -220px; margin-left:600px;text-align:left;font-size: 24px;color: orange;font-family: calibri light">Lokasi</h2> 
							<div id ="map" style="height: 253px;width: 447px; margin-left:600px;">
						</div>
						 
						</section><!--/slider-->
					</div>
	        
				</div>

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQAs5kH9M-izJqqxprIQLqwxzJxUPxMJo&callback=initMap"></script>
<script language="javascript" type="text/javascript">

    var map;
    var geocoder;
    function InitializeMap() {
        var myLatLng = {lat: -6.9119000, lng: 107.6438800}
        var latlng = new google.maps.LatLng(-6.9120857, 107.6418758);
        var myOptions =
        {
            zoom: 8,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        map = new google.maps.Map(document.getElementById("map"), myOptions);
        map.setZoom(17)
        var marker = new google.maps.Marker({
                    map: map,
                    position: myLatLng
            });
    }

    window.onload = InitializeMap;

</script>
	<?php require_once('../core/footer_utama.php');  ?>

