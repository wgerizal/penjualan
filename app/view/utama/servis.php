<?php
	require_once('../../controller/FServisController.php');
    $data = $FServisController->index();
    require_once "../core/header_utama.php"; 
?>

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<div class="row">
					<div class="col-lg-6 col-lg-offset-3">
						<center>
						<h1></h1>
						</center>
					</div>
			
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /main navbar -->

<!-- Registration form -->
				<form class="registration-form form-validate-jquery" action="../../controller/FServisController.php?func=servis" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-6 col-lg-offset-3">
							<div class="panel">
								<div class="panel-body">
									
									<?php 
							            if (isset($_SESSION["notification_servis"]) && !empty($_SESSION["notification_servis"])) {
							        ?>
							                
							                <?php 
							                    if ($_SESSION["notification_servis"] === 'success') {
							                ?>
							                        <div class="text-center">
														<div class="alert alert-success no-border">
															<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
															<span class="text-semibold">Selamat Pendaftaran Servis Anda Berhasil! </span>
													    </div>
													</div>
							                <?php 
							                    }else{
							                ?>
							                        <div class="alert alert-danger no-border">
														<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
														<span class="text-semibold">Maaf!</span> Anda gagal mendaftar.
												    </div>
							                <?php 
							                    }

							                    unset($_SESSION["notification_servis"]);
							                ?>
							                
							        <?php
							            }else{
							        ?>

							        		<div class="text-center">
												<div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
												<h5 class="content-group-lg">Form Servis <small class="display-block">Semua data harus diisi</small></h5>
											</div>

											<div class="form-group has-feedback">
												<select name="layanan_servis" id="layanan_servis" data-placeholder="Pilih Layanan Servis" class="select" required="required">
													<option></option>
					                                <option value="Panggil Ke Tempat">Panggil Ke Tempat</option>
					                                <option value="Datang Ke Toko">Datang Ke Toko</option>
					                            </select>
											</div>

					                        <div class="form-group has-feedback">
												<textarea name="keluhan" id="keluhan" class="form-control" required="required" placeholder="Keluhan"></textarea>
											</div>

											<div class="clearfix">
													<button type="submit" class="btn bg-orange-400 btn-labeled btn-labeled-right pull-right" id="btnAdd"><b><i class="icon-plus3"></i></b> Servis </button>
											</div>

							        <?php
							            }
							        ?>

								</div>
							</div>
						</div>
					</div>

	<?php require_once('../core/footer_utama.php');  ?>

	<script type="text/javascript">
		$(document).ready(function(){

	        $(".file-styled-primary").uniform({
	          fileButtonClass: 'action btn bg-blue'
	        });

	        var validator = $(".form-validate-jquery").validate({
	            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
	            errorClass: 'validation-error-label',
	            successClass: 'validation-valid-label',
	            highlight: function(element, errorClass) {
	                $(element).removeClass(errorClass);
	            },
	            unhighlight: function(element, errorClass) {
	                $(element).removeClass(errorClass);
	            },

	            // Different components require proper error label placement
	            errorPlacement: function(error, element) {

	                // Styled checkboxes, radios, bootstrap switch
	                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
	                    if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
	                        error.appendTo( element.parent().parent().parent().parent() );
	                    }
	                     else {
	                        error.appendTo( element.parent().parent().parent().parent().parent() );
	                    }
	                }

	                // Unstyled checkboxes, radios
	                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
	                    error.appendTo( element.parent().parent().parent() );
	                }

	                // Input with icons and Select2
	                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
	                    error.appendTo( element.parent() );
	                }

	                // Inline checkboxes, radios
	                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
	                    error.appendTo( element.parent().parent() );
	                }

	                // Input group, styled file input
	                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
	                    error.appendTo( element.parent().parent() );
	                }

	                else {
	                    error.insertAfter(element);
	                }
	            },
	            validClass: "validation-valid-label"
	        });

	      });
	</script>
</body>
</html>
