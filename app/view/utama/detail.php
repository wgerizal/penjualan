<?php
    require_once('../../controller/FDetailController.php');
    $data = $FDetailController->index();
    require_once "../core/header_utama.php"; 
?>


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<div class="row">
					<div class="col-lg-6 col-lg-offset-3">
						<center>
						<h3 style="margin-top: 0px;">Detail Komponen Hardware Komputer</h3>
						</center>
					</div>
					<br>
				</div>
				<div class="row" style="margin-top: 30px;background-color: #FFFFFF;padding: 20px;">
					<div class="col-md-4 col-md-offset-2">
						<center>
							<a href="#" style="color: #000000;font-size: 16px;display: block;"><?php echo $data['barang'][0]['nama_barang']; ?></a><br>
							<img src="../../../uploads/image/<?php echo $data['barang'][0]['gambar']; ?>" style="width: 100px;height: auto;">
							<div style="margin-top: 20px;">Rp. <?php echo $data['barang'][0]['harga_barang']; ?></div>
							<div>Stok - <?php echo $data['barang'][0]['jumlah']; ?></div>
						</center>
					</div>
					<div class="col-md-4">
						<div>
							<h3 style="margin-top: 0px;">Keterangan</h3>
							<p><?php echo $data['barang'][0]['keterangan_barang']; ?></p>
						</div>
					</div>
					<div class="col-md-8 col-md-offset-2" style="margin-top: 20px;">
						<center>
						<a href="index.php" class="btn btn-danger">Kembali</a>
						<form method="POST" action="../../controller/FDetailController.php?func=masukan" style="display: inline;">
							<input type="hidden" name="id_barang" value="<?php echo $data['barang'][0]['id_barang']; ?>">

							<button type="submit" class="btn btn-primary">Masukan Keranjang </button>
						</form>
						</center>
					</div>
				</div>

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<?php require_once('../core/footer_utama.php');  ?>

	<script type="text/javascript">
		$(function() {
			$('.select').select2({});
		});
	</script>

</body>
</html>
