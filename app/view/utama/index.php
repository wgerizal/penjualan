<?php
    require_once('../../controller/FIndexController.php');
    $data = $FIndexController->index();
    require_once "../core/header_utama.php"; 
?>

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<div class="row">
					<div class="col-lg-6 col-lg-offset-3">
					</div>
					<br>
					<div class="col-md-4 col-md-offset-4">
						<center>
						<div style="width: 250px;">
							<select class="select" id="kategori">
								<option value="semua">Semua Kategori</option>
								<?php
                                  foreach ($data['kategoriBarang'] as $v) {
                                    echo '<option value="kat'.$v['id_kategori_barang'].'">'.$v['nama_kategori'].'</option>';
                                  }
                                ?>
							</select>
						</div>
						</center>
					</div>
				</div>
				<div class="row" style="margin-top: 0px;">
					<?php
						foreach ($data['barang'] as $v) {
					?>
							<div class="col-md-4 col-lg-3 filter kat<?php echo $v['id_kategori_barang']; ?>" style="margin-top: 30px;">
								<center>
									<?php
									if ($v['jumlah'] != 0) {
									?>
									<a href="detail.php?id=<?php echo $v['id_barang']; ?>" style="color: #000000;font-size: 16px;display: block;"><?php echo $v['nama_barang']; ?></a><br>
									<?php
									}else{
									?>
									<a href="#" style="color: #000000;font-size: 16px;display: block;"><?php echo $v['nama_barang']; ?></a><br>
									<?php
									}
									?>
									
									<img src="../../../uploads/image/<?php echo $v['gambar']; ?>" style="width: 100px;height: auto;">
									<div style="margin-top: 20px;">Rp. <?php echo $v['harga_barang']; ?></div>
									<div>Stok - 
										<?php
									if ($v['jumlah'] != 0) {
									?>
									<?php echo $v['jumlah']; ?>
									<?php
									}else{
									?>
									Tidak Tersedia
									<?php
									}
									?>
										
									</div>
								</center>
							</div>		
					<?php
						}
					?>
					
				</div>

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<?php require_once('../core/footer_utama.php');  ?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.select').select2({});

			$( "#kategori" ).change(function() {
			  	value = $( "#kategori" ).val().toLowerCase();

			  	if(value == "semua")
				{
				    $('.filter').show('1000');
				}
				else
				{
				    $(".filter").not('.'+value).hide('3000');
				    $('.filter').filter('.'+value).show('3000');
				    
				}
			});

			if ($(".filter-button").removeClass("active")) {
				$(this).removeClass("active");
			}

			$(this).addClass("active");

		});
	</script>

</body>
</html>
