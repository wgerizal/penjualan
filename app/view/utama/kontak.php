<?php
    require_once "../core/header_utama.php"; 
?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
				 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class style="text-align:center;font-size: 24;color: orange;font-family: calibri light"=><b>Kontak Kami</b></h2>    			    				    				
				</div>			 		
			</div>    	
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class style="text-align:center;font-size: 24;color: orange;font-family: calibri light"=><b>Pertanyaan Dan Saran</b></h2> 
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6">
				                <input type="text" name="name" class="form-control" required="required" placeholder="Name">
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" class="form-control" required="required" placeholder="Subject">
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your Message Here"></textarea>
				            </div>                        
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" class="btn bg-orange-400" value="Submit">
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class style="text-align:center;font-size: 24;color: orange;font-family: calibri light"=><b>Alamat Kami</b></h2>
	    				<address>
	    					<p>Febby Komputer</p>
							<p>Jl. Ibrahim Adjie No 47</p>
							<p>Kiaracondong Bandung Blok CC-115 Bandung Trade Mall</p>
							<p>Mobile: 0821-1752-2282</p>
							<p>Email: febbykomputer@gmail.com</p>
	    				</address>
	    		
    </div><!--/#contact-page-->
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<?php require_once('../core/footer_utama.php');  ?>

</body>
</html>
