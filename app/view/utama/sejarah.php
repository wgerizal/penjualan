<?php
    require_once "../core/header_utama.php"; 
?>
	<!-- Page container -->
	 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center" style="font-family: calibri light;color: black"><b>Sejarah Toko Febby Komputer</h2></b>    			    				    				
			  </div>			 		
			</div>    	
    		<div class="row">  	
	    		<div class="col-sm-12">
	    			<div class="contact-form">
	    			  <p align="justify"; style="font-family: calibri light; font-size: 20px">Toko Feby Komputer merupakan salah satu tempat penjualan hardware komputer dan membuka jasa service komputer. Berlokasi pada Jl. Ibrahim Adjie No 47, Kiaracondong Bandung Blok CC-115 Bandung Trade Mall, Toko Feby Komputer sudah cukup lama memulai bisnisnya semenjak tahun 2014 sehingga sudah memiliki banyak pelanggan dalam menjualkan produknya dan melakukan service komputer.</p>
	    			  <br>
	    			  <p align="justify" style="font-family: calibri light; font-size: 20px">Banyak kompetitor yang lokasinya berdekatan dengan Toko Feby Komputer. Rata-rata kompetitor belum menggunakan sistem yang terkomputerisasi untuk melayani konsumen. Hal ini menjadi salah satu peluang bagi Toko Feby Komputer dalam meningkatkan keuntungan penjualan dan juga meningkatkan jasa service komputer yang ditawarkannya. Maka Dibuatlah Website ini guna memudahkan konsumen untuk melakukan proses transaksi pemesanan barang yang tersedia di Toko Febby Komputer</p>
	    			    <br />
	    		      </p>
    			  </div>
    			</div>    			
	    	</div>  
    	</div>	
    </div><!--/#contact-page-->
	    		
    </div><!--/#contact-page-->
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<?php require_once('../core/footer_utama.php');  ?>

</body>
</html>
