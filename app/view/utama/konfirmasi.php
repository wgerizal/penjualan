<?php
    require_once('../../controller/FKonfirmasiController.php');
    $data = $FKonfirmasiController->index();
    require_once "../core/header_utama.php"; 
?>

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<div class="row">
					<div class="col-lg-6 col-lg-offset-3">
						<center>
						<h3 style="margin-top: 0px;">Konfirmasi Pembayaran</h3>
						</center>
					</div>
					<br>
				</div>
				<div class="row" style="margin-top: 20px;">
					<div class="col-md-6 col-md-offset-3">
						<form action="../../controller/FKonfirmasiController.php?func=konfirmasi" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="id_pemesanan" value="<?php echo $data['pemesanan'][0]['id_pemesanan']; ?>">
							<div class="panel panel-flat">

								<div class="panel-body">

									<?php
							            if (isset($_SESSION["notification_konfirmasi"]) && !empty($_SESSION["notification_konfirmasi"])) {
							        ?>
							                
							                <?php 
							                    if ($_SESSION["notification_konfirmasi"] === 'success') {
							                ?>
							                        <div class="text-center">
														<div class="alert alert-success no-border">
															<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
															<span class="text-semibold">Upload Bukti Pembayaran Berhasil!</span>. Kami akan segera mengkonfirmasi pembayaran Anda.
													    </div>
													</div>
							                <?php 
							                    }else{
							                ?>
							                        <div class="alert alert-danger no-border">
														<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
														<span class="text-semibold">Maaf!</span> Upload Bukti Pembayaran Gagal.
												    </div>
							                <?php 
							                    }

							                    unset($_SESSION["notification_konfirmasi"]);
							                ?>
							                
							        <?php
							            }else {
							        ?>
							        	<center>
							        		<h5>Nama Pemesan : <?php echo $data['pemesanan'][0]['nama_konsumen']; ?></h5>
							        		<h5>Kode Pemesanan : <?php echo $data['pemesanan'][0]['kode_pemesanan']; ?></h5>
							        		<h5>Total yang harus dibayar : Rp. <?php echo $data['pemesanan'][0]['total_bayar']; ?></h5>
							        	</center>
							        	<hr>
							        	<div class="form-group">
											<label>Upload Bukti Pembayaran</label>
											<input type="file" class="file-styled" name="ffoto" id="ffoto">
										</div>

										<div class="text-center">
											<button type="submit" class="btn bg-orange-400" id="btnConfirm">Konfirmasi</button>
										</div>

							        <?php    	
							            }
							        ?>
									
									
									

									
								</div>
							</div>
						</form>
					</div>
				</div>
				

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<?php require_once('../core/footer_utama.php');  ?>

	<script type="text/javascript">
		$(function() {

			$( "#btnConfirm" ).click(function() {
				if ($('#ffoto').val() == "") {
					alert('File upload harus diisi!');
                    return false;
				}
                var ext = $('#ffoto').val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
                    alert('Extension input file foto tidak diperbolehkan! (contoh: .png,.jpg,.jpeg)');
                    return false;
                }
            });
		});
	</script>

</body>
</html>
