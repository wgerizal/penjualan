<?php
    require_once "../core/header_utama.php"; 
?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Registration form -->
				<form class="registration-form form-validate-jquery" action="../../controller/FDaftarController.php?func=daftar" method="POST" >
					<div class="row">
						<div class="col-lg-6 col-lg-offset-3">
							<div class="panel">
								<div class="panel-body">
									<div class="text-center">
										<div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
										<h5 class="content-group-lg">Form Daftar <small class="display-block">Semua data harus diisi</small></h5>
									</div>
									<?php
							            if (isset($_SESSION["notification_daftar"]) && !empty($_SESSION["notification_daftar"])) {
							        ?>
							                
							                <?php 
							                    if ($_SESSION["notification_daftar"] === 'success') {
							                ?>
							                        <div class="text-center">
														<div class="alert alert-success no-border">
															<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
															<span class="text-semibold">Selamat!</span> Anda sudah berhasil mendaftar.
													    </div>
													</div>
							                <?php 
							                    }else{
							                ?>
							                        <div class="alert alert-danger no-border">
														<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
														<span class="text-semibold">Maaf!</span> Anda gagal mendaftar.
												    </div>
							                <?php 
							                    }

							                    unset($_SESSION["notification_daftar"]);
							                ?>
							                
							        <?php
							            }
							        ?>

									<div class="form-group has-feedback">
										<input type="text" name="nama_konsumen" id="nama_konsumen" class="form-control" required="required" placeholder="Nama Lengkap">
									</div>

													
									<div class="form-group has-feedback">
										<input type="text" name="alamat_konsumen" id="alamat_konsumen" class="form-control" required="required" placeholder="Alamat Lengkap">
									</div>

									<div class="form-group has-feedback">
										<input type="number" name="notelp_konsumen" id="notelp_konsumen" class="form-control" required="required" placeholder="No Telepon">
									</div>

															
									<div class="form-group has-feedback">
										<input type="text" name="username" id="username" class="form-control" required="required" placeholder="Username">
									</div>

									<div class="form-group has-feedback">
										<input type="email" name="email" id="email" class="form-control" required="required" placeholder="Email">
									</div>

									<div class="form-group has-feedback">
										<input name="password" id="password" class="form-control" type="password" required="required" placeholder="Password">
									</div>

									<div class="clearfix">
										<a href="login.php" class="btn btn-default pull-left"><i class="icon-arrow-left13 position-left"></i> Login</a>
										<button type="submit" class="btn bg-orange-400 btn-labeled btn-labeled-right pull-right" ><b><i class="icon-plus3"></i></b> Daftar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /registration form -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<?php require_once('../core/footer_utama.php');  ?>
	

	<script type="text/javascript">
		$(document).ready(function(){

	        $(".file-styled-primary").uniform({
	          fileButtonClass: 'action btn bg-blue'
	        });

	        var validator = $(".form-validate-jquery").validate({
	            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
	            errorClass: 'validation-error-label',
	            successClass: 'validation-valid-label',
	            highlight: function(element, errorClass) {
	                $(element).removeClass(errorClass);
	            },
	            unhighlight: function(element, errorClass) {
	                $(element).removeClass(errorClass);
	            },

	            // Different components require proper error label placement
	            errorPlacement: function(error, element) {

	                // Styled checkboxes, radios, bootstrap switch
	                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
	                    if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
	                        error.appendTo( element.parent().parent().parent().parent() );
	                    }
	                     else {
	                        error.appendTo( element.parent().parent().parent().parent().parent() );
	                    }
	                }

	                // Unstyled checkboxes, radios
	                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
	                    error.appendTo( element.parent().parent().parent() );
	                }

	                // Input with icons and Select2
	                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
	                    error.appendTo( element.parent() );
	                }

	                // Inline checkboxes, radios
	                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
	                    error.appendTo( element.parent().parent() );
	                }

	                // Input group, styled file input
	                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
	                    error.appendTo( element.parent().parent() );
	                }

	                else {
	                    error.insertAfter(element);
	                }
	            },
	            validClass: "validation-valid-label"
	        });

	      });
	</script>

</body>
</html>
