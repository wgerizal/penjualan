<?php

    require_once('../../controller/LaporanController.php');

    $data = $LaporanController->stok();

    //$data_api = file_get_contents('http://localhost/febbykomputer.com/app/api/Barang.php');

    require_once "../core/header.php"; 

    //get data from ip local

     require_once('../../api/APIHandler.php');
    


?>

       

    <!-- Page container -->

    <div class="page-container">



        <!-- Page content -->

        <div class="page-content">



            <!-- Main content -->

            <div class="content-wrapper">



                <!-- Form validation -->

                <div class="panel panel-flat">

                  <div class="panel-heading">

                    <h5 class="panel-title"><?php echo $data['title']; ?></h5>

                    <div class="heading-elements">

                      <ul class="icons-list">

                        <li><a data-action="collapse"></a></li>

                      </ul>

                    </div>

                  </div>



                  <div class="panel-body">

                    <form class="form-horizontal form-validate-jquery" action="<?php echo $LaporanController->path_controller; ?>" method="POST" enctype="multipart/form-data">

                      <fieldset class="content-group">

                        <legend class="text-bold"></legend>



                        <input type="hidden" name="post_type" value="add">

                        <input type="hidden" name="id" value="0">

                        <input type="hidden" name="func" value="unduhLaporanStok">



                        <!-- Basic text input -->

                        <div class="form-group">

                          <label class="control-label col-lg-3">Pilih Waktu <span class="text-danger">*</span></label>

                          <div class="col-lg-9">

                            <input type="text" class="input" id="fdate" name="fdate" value="<?php echo date('Y-m-d')?>" maxlength="10" size="12">

                                s/d 

                            <input type="text" class="input" id="tdate" name="tdate" value="<?php echo date('Y-m-d')?>"  maxlength="10"  size="12">

                          </div>

                        </div>

                         <div class="form-group">

                          <label class="control-label col-lg-3">Pilih Produk <span class="text-danger">*</span></label>

                          <div class="col-lg-9">
                            <select name="produk" id="produk">
                                <option value="">Semua Produk</option>
                                <?php
                                    foreach($response as $v){
                                        echo '<option value="'.$v['id_barang'].'">'.$v['nama_barang'].'</option>';
                                    }
                                ?>
                            </select>
                           
                          </div>

                        </div>

                       

                        

                        <!-- /basic text input -->



                      </fieldset>



                      <div class="text-right">

                        <button type="submit" class="btn btn-primary" id="btnAdd">Download</button>

                      </div>

                        

                    </form>

                  </div>

                </div>

                <!-- /form validation -->

                



            </div>

            <!-- /main content -->



        </div>

        <!-- /page content -->



    </div>

    <!-- /page container -->





<?php require_once('../core/footer.php');  ?>

    

    <script type="text/javascript">

      $(document).ready(function(){

        $('#fdate').datepicker({

            format: 'yyyy-mm-dd',

            startDate: '-3d'

        });

        

        $('#tdate').datepicker({

            format: 'yyyy-mm-dd',

            startDate: '-3d'

        });

        $(".file-styled-primary").uniform({

          fileButtonClass: 'action btn bg-blue'

        });



        var validator = $(".form-validate-jquery").validate({

            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields

            errorClass: 'validation-error-label',

            successClass: 'validation-valid-label',

            highlight: function(element, errorClass) {

                $(element).removeClass(errorClass);

            },

            unhighlight: function(element, errorClass) {

                $(element).removeClass(errorClass);

            },



            // Different components require proper error label placement

            errorPlacement: function(error, element) {



                // Styled checkboxes, radios, bootstrap switch

                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {

                    if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {

                        error.appendTo( element.parent().parent().parent().parent() );

                    }

                     else {

                        error.appendTo( element.parent().parent().parent().parent().parent() );

                    }

                }



                // Unstyled checkboxes, radios

                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {

                    error.appendTo( element.parent().parent().parent() );

                }



                // Input with icons and Select2

                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {

                    error.appendTo( element.parent() );

                }



                // Inline checkboxes, radios

                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {

                    error.appendTo( element.parent().parent() );

                }



                // Input group, styled file input

                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {

                    error.appendTo( element.parent().parent() );

                }



                else {

                    error.insertAfter(element);

                }

            },

            validClass: "validation-valid-label"

        });



      });

    </script>

</body>

</html>

