<?php
    require_once('../../controller/SupplierController.php');
    $data = $SupplierController->edit();
    require_once "../core/header.php"; 
?>
       
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Form validation -->
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h5 class="panel-title">Edit Data <?php echo $data['title']; ?></h5>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                      </ul>
                    </div>
                  </div>

                  <div class="panel-body">
                    <form class="form-horizontal form-validate-jquery" action="<?php echo $SupplierController->path_controller; ?>" method="POST" enctype="multipart/form-data">
                      <fieldset class="content-group">
                        <legend class="text-bold"></legend>

                        <input type="hidden" name="post_type" value="edit">
                        <input type="hidden" name="<?php echo $SupplierController->table_primary; ?>" value="<?php echo $data[$SupplierController->table_data][0][$SupplierController->table_primary]; ?>">
                        <input type="hidden" name="func" value="save">

                        <!-- Basic text input -->
                        <div class="form-group">
                          <label class="control-label col-lg-3">Nama Supplier <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <input type="text" name="nama_supplier" id="nama_supplier" class="form-control" required="required" value="<?php echo $data[$SupplierController->table_data][0]['nama_supplier']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Email Supplier <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <input type="text" name="email_supplier" id="email_supplier" class="form-control" required="required" value="<?php echo $data[$SupplierController->table_data][0]['email_supplier']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">No Telepon Supplier <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <input type="text" name="notelp_supplier" id="notelp_supplier" class="form-control" required="required" value="<?php echo $data[$SupplierController->table_data][0]['notelp_supplier']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Alamat Supplier <span class="text-danger">*</span></label>
                          <div class="col-lg-9">
                            <input type="text" name="alamat_supplier" id="alamat_supplier" class="form-control" required="required" value="<?php echo $data[$SupplierController->table_data][0]['alamat_supplier']; ?>">
                          </div>
                        </div>
                        
                        <!-- /basic text input -->

                      </fieldset>

                      <div class="text-right">
                        <a href="index.php" class="btn btn-default" >Cancel</a>
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- /form validation -->
                

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


<?php require_once('../core/footer.php');  ?>
    
    <script type="text/javascript">
      $(document).ready(function(){

        $(".file-styled-primary").uniform({
          fileButtonClass: 'action btn bg-blue'
        });

        var validator = $(".form-validate-jquery").validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-error-label',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {

                // Styled checkboxes, radios, bootstrap switch
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                    if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo( element.parent().parent().parent().parent() );
                    }
                     else {
                        error.appendTo( element.parent().parent().parent().parent().parent() );
                    }
                }

                // Unstyled checkboxes, radios
                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo( element.parent().parent().parent() );
                }

                // Input with icons and Select2
                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo( element.parent() );
                }

                // Inline checkboxes, radios
                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent() );
                }

                // Input group, styled file input
                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo( element.parent().parent() );
                }

                else {
                    error.insertAfter(element);
                }
            },
            validClass: "validation-valid-label"
        });
      });
    </script>
</body>
</html>
