<?php
    require_once('../../controller/ServisController.php');
    $data = $ServisController->detail();
    require_once "../core/header.php"; 
?>
       
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Form validation -->
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h5 class="panel-title">Detail Data <?php echo $data['title']; ?></h5>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                      </ul>
                    </div>
                  </div>

                  <div class="panel-body">
                    <form class="form-horizontal form-validate-jquery" action="<?php echo $ServisController->path_controller; ?>" method="POST">
                      <fieldset class="content-group">
                        <legend class="text-bold"></legend>

                        <input type="hidden" name="post_type" value="add">
                        <input type="hidden" name="id" value="0">
                        <input type="hidden" name="func" value="save">
                        <!-- Basic text input -->
                        <div class="form-group">
                          <label class="control-label col-lg-3">Nama Konsumen</label>
                          <div class="col-lg-9">
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo $data['servis'][0]['nama_konsumen']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Layanan Servis</label>
                          <div class="col-lg-9">
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo $data['servis'][0]['layanan_servis']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">keluhan</label>
                          <div class="col-lg-9">
                            <textarea class="form-control" readonly="readonly"><?php echo $data['servis'][0]['keluhan']; ?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-lg-3">Status Servis</label>
                          <div class="col-lg-9">
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo $data['servis'][0]['status_servis']; ?>">
                          </div>
                        </div>
                        
                        <!-- /basic text input -->

                      </fieldset>

                      <div class="text-center">
                        <a href="index.php" class="btn btn-default">Kembali</a>
                        <?php
                          if (strtolower($data['servis'][0]['status_servis']) == 'belum selesai') {
                        ?>
                          <form action="<?php echo $ServisController->path_controller; ?>" method="POST" style="display: inline;">
                            <input type="hidden" name="id_servis" value="<?php echo $data['servis'][0]['id_servis']; ?>">
                            <input type="hidden" name="func" value="konfirmasi_servis">
                            <button type="submit" class="btn btn-info">Konfirmasi Servis Selesai</button>  
                          </form>
                          
                        <?php
                          }
                        ?>
                        
                      </div>
                    </form>
                  </div>
                </div>
                <!-- /form validation -->
                

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


<?php require_once('../core/footer.php');  ?>
    
    <script type="text/javascript">
      $(document).ready(function(){

        $(".file-styled-primary").uniform({
          fileButtonClass: 'action btn bg-blue'
        });

        var validator = $(".form-validate-jquery").validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-error-label',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {

                // Styled checkboxes, radios, bootstrap switch
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                    if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo( element.parent().parent().parent().parent() );
                    }
                     else {
                        error.appendTo( element.parent().parent().parent().parent().parent() );
                    }
                }

                // Unstyled checkboxes, radios
                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo( element.parent().parent().parent() );
                }

                // Input with icons and Select2
                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo( element.parent() );
                }

                // Inline checkboxes, radios
                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent() );
                }

                // Input group, styled file input
                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo( element.parent().parent() );
                }

                else {
                    error.insertAfter(element);
                }
            },
            validClass: "validation-valid-label"
        });

      });
    </script>
</body>
</html>
