<?php

	

	include "Database.php";



	class Barang extends Database{



		private $table = "barang";

		private $primary = "id_barang";

		private $field = array(

									'id_barang',

									'id_supplier',

									'id_kategori_barang',

									'id_stok_barang',

									'nama_barang',

									'harga_barang',

									'keterangan_barang',

									'gambar'

							  );

		private $field_update = array(

										'id_supplier',

										'id_kategori_barang',

										'nama_barang',

										'harga_barang',

										'keterangan_barang',

										'gambar'

									 );



		function __construct()

	    {

	    		

	    }



	    public function getDataAll(){

	    	$result = $this->select('*',$this->table);

	     	echo json_encode($result) ;

	    }



	    public function getDataByID($id){

	    	$result = $this->selectWhere('*',$this->table,$this->primary." = '$id'");

	     	return $result;

	    }



	    public function data_insert($data, $id_insert = false){

	    	//string field insert

	    	$field = "";

	    	$i = 0;

			$len = count($this->field);

	    	foreach ($this->field as $column) {

	    		$field = $field."`".$column."`";

	    		if ($i != ($len - 1)) {

	    			$field = $field.", ";

	    		}

	    		$i++;

	    	}



	    	//string value insert

	    	$value = "NULL, ";

	    	$i = 0;

	    	foreach ($this->field as $column) {

	    		if ($this->primary != $column) {

	    			$value = $value."'".$data[$column]."'";



	    			if ($i != ($len - 1)) {

		    			$value = $value.", ";

		    		}

	    		}

	    		

	    		$i++;

	    	}



	    	//insert

	    	if ($id_insert) {

	    		$result = 0;

	    		$id_insert_data = $this->insert($this->table,$field,$value,$id_insert);

		    	if ($id_insert_data != 0) {

		    		$result = $id_insert_data;

		    	}

	    	}else{

	    		$result = false;

		    	if ($this->insert($this->table,$field,$value,$id_insert)) {

		    		$result = true;

		    	}	

	    	}



	     	return $result;

	    }



	    public function data_edit($data){

	    	$where = $this->primary." = ".$data[$this->primary];



	    	// value edit

	    	$value = "";

	    	$i = 0;

			$len = count($this->field_update);

	    	foreach ($this->field_update as $column) {

	    		$value = $value."`".$column."` = '".$data[$column]."'";

	    		if ($i != ($len - 1)) {

	    			$value = $value.", ";

	    		}

	    		$i++;

	    	}



	    	$result = false;



	    	if ($this->update($this->table,$value,$where)) {

	    		$result = true;

	    	}

	    	

	     	return $result;

	    }





	    public function data_delete($data){

	    	$where = $this->primary." = ".$data[$this->primary];

	    	$result = false;



	    	if ($this->delete($this->table,$where)) {

	    		$result = true;

	    	}

	    	

	     	return $result;

	    }





	    // custom



	    public function getDataBarang(){

	    	$result = $this->raw("SELECT * FROM `barang` a JOIN `kategori_barang` b on a.`id_kategori_barang` = b.`id_kategori_barang` JOIN `stok_barang` c on a.`id_stok_barang` = c.`id_stok_barang` JOIN `supplier` d on a.`id_supplier` = d.`id_supplier`");

	     	return $result;

	    }



	    public function getDataBarangById($id){

	    	$result = $this->raw("SELECT * FROM `barang` a JOIN `kategori_barang` b on a.`id_kategori_barang` = b.`id_kategori_barang` JOIN `stok_barang` c on a.`id_stok_barang` = c.`id_stok_barang` JOIN `supplier` d on a.`id_supplier` = d.`id_supplier` where a.`id_barang` = ".$id);

	     	return $result;

	    }



	    public function getDataBarangByIdArray($id){

	    	$sql = "SELECT * FROM `barang` a JOIN `kategori_barang` b on a.`id_kategori_barang` = b.`id_kategori_barang` JOIN `stok_barang` c on a.`id_stok_barang` = c.`id_stok_barang` JOIN `supplier` d on a.`id_supplier` = d.`id_supplier` ";

	    	$flag = 0;

	    	foreach ($id as $v) {

	    		if ($flag == 0) {

	    			$sql = $sql."where a.`id_barang` = ".$v." ";	

	    		}else{

	    			$sql = $sql."OR a.`id_barang` = ".$v." ";	

	    		}

	    		$flag++;

	    	}

	    	$result = $this->raw($sql);

	     	return $result;

	    }




	}
$data_barang = new Barang;
$data_barang->getDataAll();


?>