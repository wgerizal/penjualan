<!DOCTYPE html>
<html>
<head>
  <title>Sistem Penjualan</title>
</head>
<body>
  <script type="text/javascript">
  	<?php
	  if (session_status() == PHP_SESSION_NONE) {
	    session_start();
	  }
	  if (isset($_SESSION['login_id']) && !empty($_SESSION['login_id'])) {
	    echo 'window.location = "app/view/dashboard";';
	  }else{
	  	echo 'window.location = "app/view/utama";';
	  }

	?>

  
  </script>
</body>
</html>